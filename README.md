Note that we don't build for common js any more, only for es, because it's
too tricky to combine with static es module imports.

(We import from `stick-js/es` in the src/ files instead of from `stick-js`
so we can take advantage of static stuff, linting etc.)

Webpack projects can import from the es modules, and node projects can too
if you run the application script as `node -r esm <file>`
