## v0.4.20

*2024-04-11

- general: `decorateRejection` updated to allow a nil argument

## v0.4.19

*2024-04-04*

- dom: deprecated `keyPressListen`, `keyPressListenPreventDefault`
- dom: added `keyDownListen`, `keyDownListenPreventDefault`

## v0.4.18

*2024-04-04*

- io: export `info`, set default bullet to start
- fetch: add `requestIsInit`

## v0.4.17

*2024-02-25*

- express: added `methodNWithMiddlewares`, `methodWithMiddlewares`, `method3WithMiddlewares`

## v0.4.16

*2024-02-25*

- minor

## v0.4.15

*2024-02-25*

- upgraded stick-js
- bilby: add `fold4`, `ifJust`, `ifNothing`, `foldMaybe`
- fetch:
  - `requestJSON`: no longer requires presence of `umsg` to be considered a user error
  - new export `noParseCodes`
  - added `noParse` as option to `requestJSON`
  - added `requestJSONStdOpts`
  - added `requestCompleteFold`
  - added `oopsOnUserError` and `oopsOnNonUserError` as options to `doApiCall`

## v0.4.14

*2024-01-17*

- bsqlite3: add pragma functions

## v0.4.13

*2024-01-13*

- select: fixed incorrect order of setting new info and printing the old one using tellSpec

## v0.4.12 (don't use)

*2024-01-13*

- select: improve debug messages, add `selectTellWithOptions`, upgrade to reselect 5, make memoize functions explicit.

## v0.4.11

*2024-01-11*

- select: we now support an `__all__` key for debugging selectors instead of
  having to specify each selector.

## v0.4.10

*2023-12-27*

- update `listen` functions in express to return the `http.Server` instance
  instead of the express ap.

## v0.4.9

*2023-12-26*

- more consistent API for express.mjs: most functions have a '3' version and
  an 'N' version, and there are now functions `method`/`method3`/`methodN`
  for generic methods.

## v0.4.8

*2023-12-25*

- export the `db` object (bsqlite3)

## v0.4.7

*2023-12-25*

- add `download` and `sendDownload` (express)
- cleanup (internal)

## v0.4.6

*2023-06-12*

- add `express` and `bsqlite3`
- general: add `decorateRejection` and `tryAsEither`
- predicate: add `(not)defined`, `if(when)Defined`, `if(when)Undefined`, `ifHasN`

## v0.4.5

*2023-01-17*

- dom: `clss` function now filters literal `true` out as well as literal `false`.

## v0.4.3

Deprecated, do not use: contains an import error in redux.mjs.

## v0.4.2

*2023-01-17*

- io: mark `sys` as experimental.

## v0.4.1

*2022-12-25*

- Removed react-s-alert.

## v0.4.0

*2022-01-20*

- *Breaking*: `requestJSON` is now called `requestJSONStd`.

- Added `foldWhenJust` and deprecated equivalent function `foldJust` (bilby).
- Updated `requestJSON` (fetch).
- Added request types & functions and `*doApiCall` (fetch).
- Added `action` and `makeReducer` (redux).
- Added `saga` and effect types (saga).

## v0.3.12

*2021-07-22*

- Updated import (internal).

## v0.3.11

*2021-07-22*

- Updated import (internal).

## v0.3.10

- Separated react-intl from react.

## v0.3.8

*2021-07-11*

- 'react-intl' was moved to peer dependencies.

## v0.3.5

*2021-07-10*

- Added more /es aliases in package.json.

## v0.3.3

*2021-07-09*

- Updated reselect import (internal).

## v0.3.2

*2021-07-09*

- Added `recoverAndBounce`.
- Added warnings to deprecated async functions in alleycat-js/general.

## v0.1.14

*2020-07-13*

- alleycat-js/general:

    - `defaultToV` is now curried.

- alleycat-js/configure:

    - Rewritten with new API. See examples.

## v0.1.13

*2020-04-26*

- alleycat-js/select:

    - `select` takes a different argument order now: the selectorName for debugging comes first.

## v0.1.12

*2020-04-12*

- alleycat-js/redux:

    - added `container`

    - the values of `selectorTable` as given to `container*` must now be values, not functions;
      i.e., selectors, not selector factories in our normal usage.

- added alleycat-js/select.

- removed alleycat-js/reselect.

## v0.1.11

*2020-04-10*

- Add `invariant` to dependencies.

## v0.1.10

*2020-04-09*

- alleycat-js/redux:

    - the `reducer` function has been overhauled (see examples), and `traceReducer` is no longer
      exported.

## v0.1.9

*2020-04-08*

- alleycat-js/react:

    - `transformIntl` and `transformIntlComponent` are no longer exported.

## v0.1.8

*2020-04-08*

- alleycat-js/general:

    - removed `remapTuples` and `remapTuplesIn`.

- alleycat-js/react:

    - updated `prepareIntl` to use `remapTuples` from stick.

## v0.1.7

*2020-04-08*

- alleycat-js/general:

    - moved `transformIntl`, `transformIntlComponent` to `react`.

- alleycat-js/react:

    - added `prepareIntl`, `prepareIntlComponent`.

## v0.1.6

*2020-04-07*

- alleycat-js/react:

    - add `loadable`.

## v0.1.5

*2020-04-07*

- alleycat-js/react:

    - `whyWhen` is now called `useWhyTell`.
    - `shouldUpdateWith`, `shouldUpdate`, `memoWithIgnoreTell`, `memoIgnoreTell` all take a logger instead of a tag now.
    - new functions: `withMemoTell`, `withMemo`, `withEffect`, `componentTell`, `component`, `containerTell`, and `container`.

## v0.1.4

*2020-04-05*

- alleycat-js/general:

    - Add `conformsTo`

- alleycat-js/redux:

    - Add `checkStore`

- alleycat-js/reduxHooks.js:

    - New. Contains `useReduxReducer` and `useSaga`.

## v0.1.3

*2020-03-31*

- alleycat-js/react:

    - Add `whyWhen`

    - Update examples.

## v0.1.2

*2020-03-30*

- Update render message in `whyYouRerender`.

## v0.1.1

*2020-03-30*

- Update license to GPL-3.0

## v0.1.0

*2020-03-30*

- Legacy (class-based) functions have been moved from `alleycat-js/react` to
  `alleycat-js/react-legacy`.

- Changes to `useMeasureWithAfter`:

    - It is now called `useMeasureWithCb`.

    - The callback function is now called with the arguments `node` and `measure`. `measure` can be
      used to repeat the measurement. The two measurement arguments have been dropped.

## v0.0.65

- Beginning of changelog.
