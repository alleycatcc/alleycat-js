/*
Enhances the history object from the 'history' package to detect the browser back-button, as long as the history API is used to navigate.

Provides a 'wasBack' argument to the listener.

Stores 'key' in the state of each node, so beware of other state using the same key.
*/

import {
  pipe, compose, composeRight,
  die, id, noop, divideBy, always, map, addIndex, ok, whenOk, dot, dot1,
  ifPredicate, whenPredicate, eq, lt, gt, ifFalse, ifTrue, concat,
  condS, cond, otherwise, guard, sprintf1, lets, blush,
  prop, dot2, exception, raise, invoke, defaultTo, ne, ifOk,
  whenTrue, join, T, F, not, tap, list, path, prepend, whenNotOk,
  assoc, notOk, toThe, isObject, isArray, mergeToM, reduce,
  letS, assocM, concatTo, assocPathM, split, againstAll, find,
  bindProp, head, side,
  flip, guardV,
  mergeM, callOn2, callOn1,
  nil,
} from 'stick-js/es'

import { ierror, } from './general.js'

const DEBUG = false

const log = /*#__PURE__*/ console | bindProp ('log')
const debugIt = /*#__PURE__*/ (...args) => log (
  ...['[debug history]', ...args]
)
const makeDebug = /*#__PURE__*/ should => (DEBUG || should) ? debugIt : noop

/*
Usage:
  import createHistory from ...
  import { makeHistory, } from 'alleycat-js/history'
  const history = createHistory ()
  const historyWrapper = history | makeHistory
  <Router history={historyWrapper.history}>
    <App historyWrapper={historyWrapper}/>
  </Router>

  // --- in App:
  componentDidMount () {
    const { historyWrapper, } = this.props
    historyWrapper.listen (
      (location, action, wasBack) => ...
    )
  }
*/

const determineEvent = /*#__PURE__*/ ({
  action, key, cursor, keyIdx, pathname, lastPathname,
}) => {
  if (action === 'PUSH') return ['push', keyIdx]
  if (action === 'REPLACE') return ['replace', keyIdx]

  const event = cursor | condS ([
    eq (key - 1) | guardV ('forward'),
    eq (key + 1) | guardV ('back'),
    otherwise | guard (() => {
      ierror ('[listen] unexpected pop: cursor, key', cursor, key)
      return 'error'
    }),
  ])
  return [event, key]
}

export const withBackDetect = /*#__PURE__*/ ({ pushOnInit=true, rejectSamePath=true, debug: debugArg=false, } = {}) => (history) => invoke (() => {
  const debug = debugArg | makeDebug
  // --- in the case of a reload, we already have state history.
  // --- start the node counting where the last one left off.
  // --- the caller should make sure to clear all nodes after this one by doing a trivial pushState
  // (the only known way).

  let blockListener = false
  let lastPathname
  let [keyIdx, cursor] = history | path (['location', 'state', 'key']) | ifOk (
    (key) => [key, key],
    () => [-1, -1]
  )
  const { push: origPush, listen: origListen, } = history

  const listen = (listener) => (location, action) => {
    const { pathname, state, } = location

    debug ('[listen] action', action, 'pathname', pathname)

    const pathChanged = pathname !== lastPathname
    lastPathname = pathname

    return lets (
      () => state | ifOk (
        ({ key, }) => {
          debug ('[listen] key', key, 'cursor', cursor)
          const [eventType, newKeyIdx] = determineEvent ({
            action, key, cursor, keyIdx,
            pathname, lastPathname,
          })

          debug ('[listen] eventType', eventType, 'pathChanged', pathChanged)
          debug ('[listen] setting cursor to key')

          cursor = key
          keyIdx = newKeyIdx

          if (action === 'POP')
            debug ('[listen] this was a pop, setting keyIdx to', keyIdx)

          return eventType
        },
        // --- a listen event without state means we're on the initial node as the result of a back.
        () => 'back',
      ),
      (eventType) => {
        if (blockListener) {
          debug ('[listen] listener is blocked, clearing flag, blocking')
          blockListener = false
          return
        }

        return listener (location, action, { type: eventType, pathChanged, })
      },
    )
  }

  return [
    () => {
      const { location, } = history
      const { pathname, state, } = location
      const keyAtInit = history | path (['location', 'state', 'key'])
      const isReload = keyAtInit | ok
      keyAtInit | sprintf1 (
        // --- back or forward can cause the site to reload if coming from a different site, or even
        // within our own app if the location bar was manually altered.
        `We're here because of a reload, or a back or a forward caused the app to mount (key is %s)`,
      ) | debug
      if (!pushOnInit) return

      // --- the old push.
      // origPush | callOn2 (this) (pathname, state)
      // --- the new (overridden) push.
      debug ('[init] pushing clone')
      history.push (pathname, state)
      debug ('[init] blocking listener & going back')
      blockListener = true
      history.go (-1)
    },

    (listener) => {
      debug ('[listen] starting listener')
      return history.listen (listener | listen)
    },

    // --- `push` and `listen` both have callers other than us (e.g. react router).
    // --- we override `push` but leave `listen` alone.
    history | mergeM ({
      push (url, state={}) {
        const key = keyIdx += 1
        const pushState = {
          ...state,
          key,
        }
        cursor = key
        debug ('[push] url', url, 'key', pushState.key)
        origPush | callOn2 (this) (url, pushState)
      },
    })
  ]
})
