import {
  pipe, compose, composeRight,
  recurry, sprintfN,
  prop, whenOk, lets, whenYes,
} from 'stick-js/es'

// --- @peer
import { createSelector, createSelectorCreator, weakMapMemoize, } from 'reselect'

import { info, defaultToV, } from './general.js'

export const selectTellWithOptions = /*#__PURE__*/ recurry (6) (
  (options) => (tellSpec) => (sliceName) => (selectorName) => (deps) => (f) => {
    const devModeChecks = tellSpec | whenYes (() => ({
      identityFunctionCheck: 'always',
      inputStabilityCheck: 'always',
    }))
    const last = {
      current: {
        dependencyRecomputations: null,
        recomputations: null,
        result: null,
      },
    }
    const selector = createSelectorCreator (options) (deps, f, { devModeChecks, })
    const tell = (hasDependencyRecomputations, hasRecomputations, hasNewResult) => {
      if (
        tellSpec !== true &&
        (tellSpec?.[sliceName] !== true) &&
        (tellSpec?.[sliceName]?.[selectorName] !== true) &&
        (tellSpec?.[sliceName]?.__all__ !== true)
      ) return
      const prefix = [sliceName, selectorName] | sprintfN ('[select %s/%s]')
      lets (
        () => hasNewResult ? ['new result', last.current.result] :
              hasRecomputations ? ['cache miss, but same result'] :
              hasDependencyRecomputations ? ['dependencies recomputed, but still a cache hit'] :
              null,
        whenOk ((ss) => info (prefix, ... ss, last.current))
      )
    }
    // --- we wrap this in a (non-memoized, so with a tiny performance penalty of one function call)
    // function which throws away the props (see below), and in the case that we have `tellSpec`,
    // prints out what it's doing.
    return !tellSpec ?
      (state, _props) => selector (state) :
      (state, _props) => {
        const recomputations = selector.recomputations ()
        const dependencyRecomputations = selector.dependencyRecomputations ()
        const result = selector.lastResult ()
        const lc = { ... last.current }
        last.current = {
          dependencyRecomputations,
          recomputations,
          result,
        }
        tell (
          dependencyRecomputations !== lc.dependencyRecomputations,
          recomputations !== lc.recomputations,
          result !== lc.result,
        )
        return selector (state)
      }
  }
)

export const selectTell = /*#__PURE__*/ selectTellWithOptions ({
  // --- these are the defaults since reselect 5, but just to make it explicit.
  memoize: weakMapMemoize,
  argsMemoize: weakMapMemoize,
})

export const initSelectorsTell = /*#__PURE__*/ recurry (3) (
  (tellSpec) => (sliceName) => (initialState) => {
    // --- we throw the props away to greatly simplify memoization.
    // --- if you really need to pass them to the selector, you can either have the selector return
    // a function which accepts the needed props as arguments, or use createSelector directly.
    const selectSlice = (store, _props) => store | prop (sliceName) | defaultToV (initialState)

    const select = selectTell (tellSpec, sliceName)
    const selectTop = (selectorName, f) => select (selectorName, [selectSlice], f)
    const selectVal = (propName) => selectTop (propName, prop (propName))

    return {
      select,
      selectTop,
      selectVal,
    }
  }
)

export const initSelectors = /*#__PURE__*/ null | initSelectorsTell
