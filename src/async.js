import {
  pipe, composeRight, compose,
  dot, prop, dot1,
  recurry,
  tap, always,
} from 'stick-js/es'

import { Left, Right, } from './bilby.js'

const V = /*#__PURE__*/ void 8 | always

export const then = /*#__PURE__*/ dot1 ('then')
export const recover = /*#__PURE__*/ dot1 ('catch')

export const thenTap = /*#__PURE__*/ recurry (2) (
  f => p => p.then ((...args) => (f (...args), p))
)

/*
 * Usage: at the end of a promise chain:
 *
 * e.g.
 *
 *  fetch (url, options)
 *  | then/recover (...)
 *  | then/recover (...)
 *  | ...
 *  | promiseToEither
 *
 * e.g. in a saga:
 *
 *  const results = yield fetch (url, options)
 *    | then/recover (...)
 *    | ...
 *    | promiseToEither
 *
 * which results is an Either.
 */

export const promiseToEither = /*#__PURE__*/ then (Right) >> recover (Left)

export const resolveP = /*#__PURE__*/ (val) => Promise.resolve (val)
export const rejectP  = /*#__PURE__*/ (val) => Promise.reject (val)
export const allP     = /*#__PURE__*/ (ps)  => Promise.all (ps)
export const startP   = /*#__PURE__*/ ()    => null | resolveP

/* Catch a promise rejection, run `f`, and then re-reject it.
 * Useful for e.g. error messages.
 * Note that we must be sure to return undefined, so that the promise doesn't automatically resolve.
 */
export const recoverAndBounce = /*#__PURE__*/ (f) => recover (tap (f) >> rejectP >> V)
