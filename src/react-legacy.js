import {
  pipe, compose, composeRight,
  recurry,
  prop, tap, ok, whenOk, dot, dot1,
  eq, ifFalse, ifTrue, noop,
  whenPredicate, sprintf1,
  not, merge, letS, lets,
  side, mergeTo,
  bindProp,
  join, sprintfN,
  isObject, ifPredicate,
  map,
  appendToM,
  condS, guard,
  isString, isArray, isFunction, otherwise,
  side1, add, plus, minus, always,
  arg0, arg2, deconstruct2,
  assocM,
  id,
  invoke,
  ifOk,
  concatTo,
} from 'stick-js/es'

// --- @peer
import React from 'react'
const { PureComponent, } = React

import reactResizeDetector from 'react-resize-detector'
const { withResizeDetector, } = reactResizeDetector

import { truncate, } from './general.js'
import { whyYouRerender, } from './react.js'

// ------ classes (old-style).

export const createRef = /*#__PURE__*/ React | bindProp ('createRef')

; `this | setState ({ query, })`
export const setState = /*#__PURE__*/ side1 ('setState')
; `this | forceUpdate`
export const forceUpdate = /*#__PURE__*/ side ('forceUpdate')

/*
Create a higher-order component which re-renders on window resize, and which calls the function
\`onResize\` if it was passed as a prop.

Note that this will also inject props 'width' and 'height' into the component.

It might have been nice to create a ref here and pass it to the handler, but that's difficult
because refs work differently for classes & stateless components and it's not clear where we would
attach it.

We also don't do any size calculations because there are so many ways to do it, not everything is
cross-browser, etc, but this could be added in the future.
*/

export const withResizeListener = /*#__PURE__*/ (Component) => lets (
  () => class extends PureComponent {
    onResize () {
      const { props, } = this
      const { whenResize, width, height, } = props
      if (whenResize) return whenResize ({ width, height, })
    }

    componentDidUpdate (prevProps) {
      return this.onResize ()
    }

    componentDidMount () {
      return this.onResize ()
    }

    render () {
      const { props, } = this
      return React.createElement (Component, props)
    }
  },
  // --- withResizeDetector (react-resize-detector) forces a re-render if the element is resized.
  // --- we ignore the props it provides us further.
  (ComponentHOC) => ComponentHOC | withResizeDetector,
)

/*
Provide props and/or state keys, changes to which should *not* cause a re-render. Comparison is shallow.

Usage:
  class SomeComponent extends Component {
    shouldUpdate = shouldUpdateShallow ([somePropKeys, ...], [someStateKeys, ...])
    shouldComponentUpdate = (nextProps, nextState) => {
      const { props, state, shouldUpdate, } = this
      return shouldUpdate (
        [nextProps, props],
        [nextState, state], // optional
      )
    }
    // --- or
    shouldComponentUpdate = this | shouldComponentUpdate (this.shouldUpdate)
  }

  shouldUpdateShallowExplain returns the first prop or state key which is new, or null if nothing is new.

  For debugging info on what updated, similar to whyYouRerender:

  shouldUpdate = shouldUpdateShallowExplain ([somePropKeys, ...], [someStateKeys, ...])
    // --- tap some kind of logging function, like:
    >> tap (logWith ('component updating because:'))
    >> Boolean
*/

export const shouldUpdateShallowExplain = /*#__PURE__*/ (skipKeysProps, skipKeysState=[]) => {
  const skipKeysPropsSet = new Set (skipKeysProps)
  const skipKeysStateSet = new Set (skipKeysState)
  return ([nextProps, props], [nextState, state]=[[], []]) => {
    for (const p in nextProps)
      if (!skipKeysPropsSet.has (p) && nextProps [p] !== props [p])
        return p
    for (const s in nextState)
      if (!skipKeysStateSet.has (s) && nextState [s] !== state [s])
        return s
    return null
  }
}

export const shouldUpdateShallow = /*#__PURE__*/ (...args) => shouldUpdateShallowExplain (...args) >> Boolean

/* Usage: see shouldUpdateShallow
*/

export const shouldComponentUpdate = /*#__PURE__*/ (shouldUpdate) =>
  (self) => (nextProps, nextState) => self | compMeth (
    (props, state) => shouldUpdate (
      [nextProps, props], [nextState, state],
    )
  )

/*
Usage:
  render = () => this | compMeth ((
    { prop1, prop2, ... },
    { state-key1, state-key2, ... },
    { this-key1, this-key2, ... },
    ) => ...
  )
*/

export const compMeth = /*#__PURE__*/ recurry (2) (
  f => deconstruct2 (
    ({ props, state, }) => self => f (props, state || {}, self),
  ),
)

/* Usage:

In classes, using the 'class properties' feature:

  Use the forms with 'Meth' to assign a method directly:

  onClick = this | incrementStateValueMeth ('searchRowsKey')

  Use the forms without 'Meth' inside a function:

  onClick = () => this | incrementStateValue ('searchRowsKey')

In ordinary objects:

  onClick () {
    return this | incrementStateValue ('searchRowsKey')
  }

Example:

  state = {
    color: undefined,
    show: false,
    childKey: 0,
  }
  setColor = this | setStateValueMeth ('color')
  showHide = this | toggleStateValueMeth ('show')
  rerenderChild = this | incrementStateValueMeth ('childKey')
  onClickBlue = () => this.setColor ('blue')
  onClickRed = () => this.setColor ('red')
  render = () => {
    const { onClickBlue, onClickRed, } = this
    <Button onClick={onClickBlue}/>
    <Button onClick={onClickRed}/>
    <Button onClick={showHide}>
    <Button onClick={rerenderChild}>
  }

*/

export const setStateValueMeth = /*#__PURE__*/ recurry (3) (
  key => self => value => self | setState ({
    [key]: value,
  }),
)

/* Note: curried at 3, so that the last part (_) can be called with an arbitrary
 * argument list.
 */
export const updateStateValueMeth = /*#__PURE__*/ recurry (3) (
  f => key => self => _ => self | setState ({
    [key]: self.state [key] | f,
  })
)

export const toggleStateValueMeth = /*#__PURE__*/ not | updateStateValueMeth
export const incrementStateValueMeth = /*#__PURE__*/ 1 | plus | updateStateValueMeth

export const setStateValue = /*#__PURE__*/ recurry (3) (
  key => value => self => setStateValueMeth (key, self, value),
)
export const toggleStateValue = /*#__PURE__*/ recurry (2) (
  key => self => toggleStateValueMeth (key, self) (null),
)
export const incrementStateValue = /*#__PURE__*/ recurry (2) (
  key => self => updateStateValueMeth (1 | plus, key, self) (null),
)

/*
Usage:

In component named e.g. Header:

  componentDidUpdate = this | whyUpdateMeth ('Header', debugRender)

or

  componentDidUpdate = (prevProps, prevState) => {
    whyUpdateMeth ('Header', debugRender) (this) (prevProps, prevState)
    ...
  }

`debugRender` is optional and defaults to true.
*/

export const whyUpdateMeth = /*#__PURE__*/ recurry (6) (
  tag => doCheck => self => prevProps => prevState => snapshot => self | compMeth (
    (props, state) => doCheck && whyYouRerender (
      tag, [prevProps, props], [prevState, state],
    ),
  )
)

export const whyUpdate = /*#__PURE__*/ recurry (6) (
  tag => doCheck => prevProps => prevState => snapshot => self =>
    whyUpdateMeth (tag, doCheck, self, prevProps, prevState),
)

