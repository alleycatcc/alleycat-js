import {
  pipe, compose, composeRight,
  side1, side2, side3, side4, sideN,
  dot1, dot2, dot3, dot4,
  recurry,
} from 'stick-js/es'

// ------ app

export const _appFnN = /*#__PURE__*/ recurry (5) (
  (middlewares) => (fnName) => (path) => (fs) => (app) => (app [fnName] (path, ... middlewares, ... fs), app),
)
export const _appFn = /*#__PURE__*/ recurry (5) (
  (middlewares) => (fnName) => (path) => (f) => _appFnN (middlewares, fnName, path, [f]),
)
export const _appFn3 = /*#__PURE__*/ recurry (6) (
  (middlewares) => (fnName) => (path) => (f) => (g) => _appFnN (middlewares, fnName, path, [f, g]),
)

export const methodNWithMiddlewares = /*#__PURE__*/ _appFnN
export const methodWithMiddlewares = /*#__PURE__*/ _appFn
export const method3WithMiddlewares = /*#__PURE__*/ _appFn3
export const methodN = /*#__PURE__*/ methodNWithMiddlewares ([])
export const method = /*#__PURE__*/ methodWithMiddlewares ([])
export const method3 = /*#__PURE__*/ method3WithMiddlewares ([])
export const all = /*#__PURE__*/ method ('all')
export const all3 = /*#__PURE__*/ method3 ('all')
export const allN = /*#__PURE__*/ methodN ('all')
// --- `delete` is a JS keyword, hence `del`
export const del = /*#__PURE__*/ method ('delete')
export const del3 = /*#__PURE__*/ method3 ('delete')
export const delN = /*#__PURE__*/ methodN ('delete')
export const get = /*#__PURE__*/ method ('get')
export const get3 = /*#__PURE__*/ method3 ('get')
export const getN = /*#__PURE__*/ methodN ('get')
export const patch = /*#__PURE__*/ method ('patch')
export const patch3 = /*#__PURE__*/ method3 ('patch')
export const patchN = /*#__PURE__*/ methodN ('patch')
export const post = /*#__PURE__*/ method ('post')
export const post3 = /*#__PURE__*/ method3 ('post')
export const postN = /*#__PURE__*/ methodN ('post')
export const put = /*#__PURE__*/ method ('put')
export const put3 = /*#__PURE__*/ method3 ('put')
export const putN = /*#__PURE__*/ methodN ('put')

// --- `listen` is special because it's intended as the last step in the chain, so it doesn't pass
// the app object through like the others do but returns the underlying `http.Server` instance.
export const listen1 = /*#__PURE__*/ dot1 ('listen')
export const listen2 = /*#__PURE__*/ dot2 ('listen')
export const listen3 = /*#__PURE__*/ dot3 ('listen')
export const listen4 = /*#__PURE__*/ dot4 ('listen')
export const listen = /*#__PURE__*/ listen2

export const param = /*#__PURE__*/ side2 ('param')
export const param1 = /*#__PURE__*/ side1 ('param')
export const route = /*#__PURE__*/ side1 ('route')
export const setSetting = /*#__PURE__*/ side2 ('set')
export const getSetting = /*#__PURE__*/ side1 ('get')
export const usePathN = /*#__PURE__*/ _appFnN ([]) ('use')
export const usePath = /*#__PURE__*/ _appFn ([]) ('use')
export const usePath3 = /*#__PURE__*/ _appFn3 ([]) ('use')
export const use = /*#__PURE__*/ side1 ('use')
export const useN = /*#__PURE__*/ sideN ('use')

// ------ response

export const download = /*#__PURE__*/ side3 ('download')
export const send = /*#__PURE__*/ side1 ('send')
export const sendJSON = /*#__PURE__*/ side1 ('json')
// --- send null body.
export const sendEmpty = /*#__PURE__*/ send (null)
// --- only set status.
export const status = /*#__PURE__*/ side1 ('status')
// --- set status and send a string.
export const sendStatusRaw = /*#__PURE__*/ recurry (3) (
  (code) => (body) => status (code) >> send (body),
)
export const sendDownload = /*#__PURE__*/ recurry (4) (
  (path) => (downloadAs) => (onErr) => download (path, downloadAs, onErr),
)
// --- set status and send JSON.
export const sendStatus = /*#__PURE__*/ recurry (3) (
  (code) => (data) => status (code) >> sendJSON (data),
)
export const sendStatusEmpty = /*#__PURE__*/ recurry (2) (
  (code) => status (code) >> sendEmpty,
)
