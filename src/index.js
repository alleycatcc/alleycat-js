/* A dummy index.js file, to avoid breaking code which assumes an entry point (in particular the
 * webpack dll builder).
 *
 * We also don't import and reexport all the library's symbols here. They need to be imported from
 * the various modules.
 */
