/* usage:

// --- parent or component
//
// @todo: update example to new-style

const fontLora = fontFace (
  require ('../../fonts/lora.woff2'),
  'woff2',
  'Lora',
)

const fontIndieFower = fontFace (
  'https://fonts.gstatic.com/s/indieflower/v10/m8JVjfNVeKWVnh3QMuKkFcZVaUuC.ttf',
  'truetype',
  'Indie Flower',
  { weight: 600 },
)

const fontIndieFlower = cssFont (
  'https://fonts.googleapis.com/css?family=Indie+Flower',
)
*/

import {
  pipe, compose, composeRight,
  ifTrue, sprintf1, ifFalse,
  sprintfN, compactOk, whenOk,
  die, join, map, invoke,
  lets,
} from 'stick-js/es'

import daggy from 'daggy'

import { cata, } from './bilby.js'

const FontSource = /*#__PURE__*/ daggy.taggedSum ('FontSource', {
  FontSourceLocal: ['name'],
  FontSourceURL: ['url', 'format'],
})

const fontSourceShow = /*#__PURE__*/ cata ({
  FontSourceLocal: (name) => name | sprintf1 ("local('%s')"),
  FontSourceURL: (url, format) => [url, format] | sprintfN (`url("%s") format("%s")`),
})

const { FontSourceLocal, FontSourceURL, } = FontSource

const FontSources = /*#__PURE__*/ daggy.tagged ('FontSources', ['sources'])
const showFSMapper = /*#__PURE__*/ map (fontSourceShow) >> join (',\n')
const showFontSources = /*#__PURE__*/ fs => fs.sources | showFSMapper | sprintf1 ('src: %s;')

const attrib = /*#__PURE__*/ name => whenOk (val => ([name, val]) | sprintfN (
  '      %s: %s;'
))

// --- `unicodeRange` should be given as e.g. 'U+000-FFF'
export const fontFace = /*#__PURE__*/ lets (
  () => `
    @font-face {
      %s
    }
  `,
  (fmt) => (family, specs, { style=null, weight=null, stretch=null, unicodeRange=null, } = {}) => [
    family | attrib ('font-family'),
    showFontSources (FontSources ([
      FontSourceLocal (family),
      ...(specs | map (
        ([url, family]) => FontSourceURL (url, family),
      )),
    ])),
    style | attrib ('font-style'),
    weight | attrib ('font-weight'),
    stretch | attrib ('font-stretch'),
    unicodeRange | attrib ('unicode-range'),
  ]
    | compactOk
    | join ('\n')
    | sprintf1 (fmt),
)

export const cssFont = /*#__PURE__*/ (url) => {
  const fmt = `
    @import url('%s');
  `
  return url | sprintf1 (fmt)
}
