import {
  pipe, compose, composeRight,
} from 'stick-js/es'

import daggy from 'daggy'

// --- inject the redux-saga/effects module to solve weird import issues.
export const saga = /*#__PURE__*/ (sagaEffects, taker, action, f) => {
  const { call, } = sagaEffects
  function *g ({ data, }) {
    yield call (f, data)
  }
  return taker (action.c, g)
}

const Eff = daggy.taggedSum ('Eff', {
  EffAction: ['action'],
  EffSaga: ['f'],
  EffNoEffect: [],
})

const { EffAction, EffSaga, EffNoEffect, } = Eff
export { EffAction, EffSaga, EffNoEffect, }
