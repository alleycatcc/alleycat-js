// --- predicate has no alleycat-js dependencies.

import {
  pipe, composeRight, compose,
  prop, againstAll,
  ifPredicate, eq, gt,
  find,
  getType,
  isType,
  isObject, isArray,
  ifTrue, always, ifOk, ifFalse, ifNotOk,
  noop,
  whenPredicate, not,
  whenTrue, whenFalse,
  dot, ok, nil,
  recurry, has,
  ne,
  ifPredicateV, whenPredicateV,
  allAgainst,
  keys, list, anyAgainst,
} from 'stick-js/es'

const length = /*#__PURE__*/ prop ('length')

// --- @todo
const isLeft = noop

export const ifLongerThan = /*#__PURE__*/ n => ifPredicate (length >> gt (n))

export const isException = /*#__PURE__*/ isType ('Error')
export const ifException = /*#__PURE__*/ ifPredicate (isException)
export const ifNegativeOne = /*#__PURE__*/ ifPredicate (-1 | eq)

export const ifSingletonLeft = /*#__PURE__*/ ifPredicate (
  againstAll ([
    isArray,
    length >> eq (1),
    prop (0) >> isLeft
  ])
)

export const ifArray = /*#__PURE__*/ isArray | ifPredicate
export const ifObject = /*#__PURE__*/ isObject | ifPredicate
export const isNull = /*#__PURE__*/ null | eq
export const ifNull = /*#__PURE__*/ ifPredicate (isNull)
export const isTrue = /*#__PURE__*/ true | eq
export const isFalse = /*#__PURE__*/ false | eq

// --- @todo move to stick?
export const complement = /*#__PURE__*/ f => (...args) => f (...args) | not
export const whenNotPredicate = /*#__PURE__*/ complement >> whenPredicate

export const ifTrueV = /*#__PURE__*/ isTrue | ifPredicateV
export const ifFalseV = /*#__PURE__*/ isFalse | ifPredicateV
export const ifOkV = /*#__PURE__*/ ok | ifPredicateV
export const ifNotOkV = /*#__PURE__*/ nil | ifPredicateV
export const whenTrueV = /*#__PURE__*/ isTrue | whenPredicateV
export const whenFalseV = /*#__PURE__*/ isFalse | whenPredicateV

// --- assumes that it *is* a list, and checks the length.
export const isEmptyList = /*#__PURE__*/ prop ('length') >> eq (0)
export const isNotEmptyList = /*#__PURE__*/ isEmptyList >> not

// --- assumes that it *is* an object, and checks the keys.
export const isEmptyObject = /*#__PURE__*/ keys >> isEmptyList
export const isNotEmptyObject = /*#__PURE__*/ isEmptyObject >> not

// --- assumes that it *is* a string, and checks it.
export const isEmptyString = /*#__PURE__*/ isEmptyList
export const isNotEmptyString = /*#__PURE__*/ isEmptyString >> not
export const isWhiteSpaceOrEmpty = /*#__PURE__*/ dot ('trim') >> isEmptyString

export const ifEmptyList = /*#__PURE__*/ isEmptyList | ifPredicate
export const ifEmptyString = /*#__PURE__*/ isEmptyString | ifPredicate

// @todo eq >> ifPredicate
export const ifEquals = /*#__PURE__*/ recurry (3) (
  x => ifPredicate (x | eq),
)

// @todo recurry
export const whenEquals = /*#__PURE__*/ eq >> whenPredicate
export const ifEqualsV = /*#__PURE__*/ x => ifPredicateV (x | eq)
export const whenEqualsV = /*#__PURE__*/ eq >> whenPredicateV

export const ifNotEquals = /*#__PURE__*/ recurry (3) (
  x => ifPredicate (x | ne),
)

// @todo recurry
export const whenNotEquals = /*#__PURE__*/ ne >> whenPredicate
export const ifNotEqualsV = /*#__PURE__*/ x => ifPredicateV (x | ne)
export const whenNotEqualsV = /*#__PURE__*/ ne >> whenPredicateV

// @stick
export const ifPredicateResult = /*#__PURE__*/ recurry (4) (
  (f) => (yes) => (no) => (x) => {
    const p = f (x)
    return p ? yes (x, p) : no (x)
  }
)
export const whenPredicateResult = /*#__PURE__*/ recurry (3) (
  f => yes => ifPredicateResult (f) (yes) (noop)
)

export const notTrue = /*#__PURE__*/ true | ne
export const whenNotTrue = /*#__PURE__*/ notTrue | whenPredicate
export const whenNotTrueV = /*#__PURE__*/ x => whenNotTrue (_ => x)
export const ifNotTrue = /*#__PURE__*/ notTrue | ifPredicate

export const whenNotEmptyString = /*#__PURE__*/ isNotEmptyString | whenPredicate
export const whenNotEmptyList = /*#__PURE__*/ isNotEmptyList | whenPredicate

export const okAndNotFalse = /*#__PURE__*/ againstAll ([ok, false | ne])
export const ifOkAndNotFalse = /*#__PURE__*/ okAndNotFalse | ifPredicate

export const ifType = /*#__PURE__*/ isType >> ifPredicate

export const bothOk = /*#__PURE__*/ (x, y) => [x, y] | allAgainst (ok)
export const allOk = /*#__PURE__*/ allAgainst (ok)
export const whenAllOk = /*#__PURE__*/ allOk | whenPredicate
export const ifAllOk = /*#__PURE__*/ allOk | ifPredicate

/* `all` and `any` are similar to `lets`.
 *
 * `all` is like `lets` with an abort: if any of the entries returns falsey, it stops.
 * `any` stops on the first truthy values it encounters.
 *
 * The `all` variants return the last evaluated value (or `false`) instead of returning `true`.
 * The `any` variants return the value (or `false`) instead of returning `true`.
 */

export const allV = /*#__PURE__*/ list >> allAgainst (Boolean)
export const anyV = /*#__PURE__*/ list >> anyAgainst (Boolean)

export const all = /*#__PURE__*/ (...fs) => {
  const acc = []
  let last
  for (const f of fs) {
    const ret = f (...acc)
    if (!ret) return false
    acc.push (ret)
    last = ret
  }
  return last
}

// --- @todo return value, not true
export const any = /*#__PURE__*/ (...fs) => {
  const acc = []
  for (const f of fs) {
    const ret = f (...acc)
    if (ret) return true
    acc.push (ret)
  }
  return false
}

export const allN = /*#__PURE__*/ xs => all (...xs)
export const ifAllN = /*#__PURE__*/ recurry (3) (
  (ja) => (nee) => allN >> ifTrue (ja) (nee),
)
export const whenAllN = /*#__PURE__*/ recurry (2) (
  (ja) => allN >> whenTrue (ja),
)

export const allNV = /*#__PURE__*/ xs => allV (...xs)
export const ifAllNV = /*#__PURE__*/ recurry (3) (
  (ja) => (nee) => allNV >> ifTrue (ja) (nee),
)
export const whenAllNV = /*#__PURE__*/ recurry (2) (
  (ja) => allNV >> whenTrue (ja),
)

// @experimental, can't curry.
export const ifAll = /*#__PURE__*/ (ja) => (nee) => (...xs) => all (...xs) | ifTrue (ja) (nee)
export const whenAll = /*#__PURE__*/ (ja) => (...xs) => all (...xs) | whenTrue (ja)

export const defined = /*#__PURE__*/ void 8 | ne
export const notDefined = /*#__PURE__*/ defined >> not
export const ifDefined = /*#__PURE__*/ defined | ifPredicate
export const ifUndefined = /*#__PURE__*/ notDefined | ifPredicate
export const whenDefined = /*#__PURE__*/ defined | whenPredicate
export const whenUndefined = /*#__PURE__*/ notDefined | whenPredicate

// --- @todo integrate ifHas etc. from kattenluik with stick, and then move this to stick.
export const ifHasN = /*#__PURE__*/ recurry (4) (
  (keys) => (yes) => (no) => (o) => {
    const values = []
    for (const k of keys) {
      if (!has (k, o)) return no (o, keys)
      values.push (o [k])
    }
    return yes (values, o, keys)
  }
)
