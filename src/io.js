import fs from 'fs'

import {
  pipe, composeRight, compose,
  always, join, tap,
  dot2, ifFalse, sprintfN,
  concatTo, concat,
  compactOk, compact,
  lets,
  exception, raise, bindPropTo,
  timesV, recurry, sprintf1,
} from 'stick-js/es'

import fishLib from 'fish-lib'
const {
  log, info, warn, error,
  green, yellow, magenta, red, brightRed, cyan, brightBlue, blue,
  sysSpawn,
} = fishLib

fishLib.sysSet ({ sync: false, die: false, verbose: false, })
fishLib.bulletSet ('٭')

const on = /*#__PURE__*/ dot2 ('on')

// --- @experimental
// - This throws if the command fails to start -- maybe it should reject instead?
// - Only works for async
// - Err gets eaten

export const sys = /*#__PURE__*/ (...args) => new Promise ((res, rej) => {
  let ret
  const sysArgs = args | concat ([
    ({ out, ok, err, }) => ok | ifFalse (
      _ => "cmd failed" | exception | raise,
      _ => ret = out,
    )
  ])
  sysSpawn (...sysArgs)
  | on ('close') ((code, signal) => code === 0
    ? ret | res
    : code | sprintf1 ('cmd error (code = %d)') | rej
  )
  | on ('error') (rej)
})

export const writeFile = /*#__PURE__*/ recurry (2) (
  path => contents => fs.writeFileSync (path, contents),
)

export const write = /*#__PURE__*/ 'write' | bindPropTo (process.stdout)

export const appendToFile = /*#__PURE__*/ recurry (2) (
  filename => contents => fs.appendFileSync (filename, contents),
)

export {
  log, info, warn, error,
  green, yellow, magenta, red, brightRed, cyan, brightBlue, blue,
}

export const goUp = /*#__PURE__*/ '[A' | always

const spinner = {
  job: void 8,
  charIdx: 0,
  chars: "◓◑◒◐",
  label: '',
  lastNumChars: 0,
  cycleChar () {
    this.charIdx = ++this.charIdx % this.chars.length
  },
  str () {
    return lets (
      _ => '' | timesV (this.lastNumChars) | join (''),
      _ => this.chars [this.charIdx],
      _ => this.label,
      (pref, char, label) => [char, label, char]
        | sprintfN ('%s %s %s')
        | tap (l => this.lastNumChars = l.length)
        | concatTo (pref)
    )
    | tap (_ => this.cycleChar ())
  },
  start (label) {
    this.label = label
    this.job = setInterval (
      _ => this.str () | write,
      100,
    )
  },
  stop () {
    clearInterval (this.job)
  },
}

export const startSpinner = /*#__PURE__*/ 'start' | bindPropTo (spinner)
export const stopSpinner = /*#__PURE__*/ 'stop' | bindPropTo (spinner)

export const showCursor = /*#__PURE__*/ _ => '\u001b[?25h' | write
export const hideCursor = /*#__PURE__*/ _ => '\u001b[?25l' | write
