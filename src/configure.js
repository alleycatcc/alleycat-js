/*
 * Usage:
 *
 * Given some config object like
 *
 *   const config = {
 *     colors: {
 *       main: {
 *         color1: 'red',
 *         color2: 'green',
 *       },
 *     },
 *   }
 *
 * You can get the values in various ways:
 *
 *   const configTop = configure.init (config)
 *   const color1 = configTop.get ('colors.main.color1')
 *   // --- you must use the array form if keys might contain a dot.
 *   // for the rest, the two forms are interchangeable in the examples below.
 *   const color1 = configTop.get (['colors', 'main', 'color1'])
 *   const { color1, color2, } = configTop.gets ({
 *     color1: 'colors.main.color1',
 *     color2: 'colors.main.color2',
 *   })
 *   const mainColors = configTop.focus ('colors.main')
 *   const color1 = mainColors.get ('color1')
 *   const color2 = mainColors.get ('color2')
 *   const { color1, color2, } = mainColors.gets ({
 *     color1: 'color1',
 *     color2: 'color2',
 *   })
 *   // --- there's a special convenience shorthand form of `gets` when you're at the last level and
 *   the path has only 1 component:
 *
 *   const { color1, color2, } = mainColors.gets ('color1', 'color2')
 *   // --- `gets` returns an object, not an array, because an array isn't that convenient when you're
 *   // destructuring a lot of values at a time (say more than 5).
 *   // To use an array idiom:
 *   const [color1, color2] = ['color1', 'color2'] | map (col.get)
 *
 * Trying to get a value which is undefined or null will result in an 'oops' bubble and a message in
 * the console.
 *
 * A config object is expected to contain a fixed set of keys. It's always considered an error if a
 * key is non-existent or undefined. It's not possible to check if a key is present without
 * triggering one of the error or warning paths.
 *
 * Values are allowed to be `null`, but not undefined. This will result in a run-time error.
 *
 * Use this at the top of the module files, so it quits as early as possible on missing key.
 */


import {
  pipe, compose, composeRight,
  join, map, sprintfN,
  compact, match, guard, otherwise,
  ifTrue, ifFalse,
  ifPredicate, gt, noop,
  defaultTo, ok, eq, dot, dot1,
  list,
  divideBy,
  path, split,
  letS, lets,
  mapValues,
  invoke,
  id, sprintf1, die,
} from 'stick-js'

import {
  ifObject, ifArray,
} from './predicate.js'

import {
  mergeAll,
} from './general.js'

const isUndefined = /*#__PURE__*/ void 8 | eq
const ifUndefined = /*#__PURE__*/ isUndefined | ifPredicate
const orElse = /*#__PURE__*/ (f) => ifUndefined (f, id)

/* curLoc: array
 * thePath: array
 */
const configGetIn = /*#__PURE__*/ (curLoc) => (config) => (thePath) => config
  | path (thePath)
  | orElse (
    _ => [[...curLoc, ...thePath] | join ('/')]
      | sprintfN ("Couldn't get config key %s")
      | die
  )

const getPath = /*#__PURE__*/ (get) => ifArray (
  get,
  split ('.') >> get,
)

const errMsg = /*#__PURE__*/ invoke (() => {
  const toArray = ifArray (
    id,
    x => [x],
  )

  return toArray
  >> join ('/')
  >> sprintf1 ("Couldn't get config key: %s")
})

const _configGetsMapper = /*#__PURE__*/ (_configGet) => (config) => lets (
  () => config | _configGet,
  (get) => ifObject (
    get | mapValues,
    key => ({ [key]: key | get }),
  )
)

// --- config -> key | [key] -> val
const configGet = /*#__PURE__*/ (curLoc) => (config) => lets (
  () => config | configGetIn (curLoc),
  (get) => get | getPath,
)

const configGets = /*#__PURE__*/ (curLoc) => (config) => lets (
  () => config | _configGetsMapper (configGet (curLoc)),
  (theMapper) => list >> map (theMapper) >> mergeAll,
)

/* curLoc: array,
 * 'tell' because `curLoc` is used for remembering where we are for a nicer error message.
 */
const focusTell = /*#__PURE__*/ (curLoc) => (config) => lets (
  () => config | configGet (curLoc),
  () => config | configGets (curLoc),
  (get) => (spec) => get (spec) | focusTell ([...curLoc, spec]),
  (get, gets, focus) => ({
    get, gets, focus,
  }),
)

export default {
  init: focusTell ([]),
}
