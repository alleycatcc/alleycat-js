import {
  pipe, composeRight, compose,
  each, path, prop,
  recurry, dot1, tap, noop,
  list, compact, join, side, dot,
  side2, filter, lets, not,
} from 'stick-js/es'

import scrollIntoViewMod from 'scroll-into-view-if-needed'

import { whenEquals, } from './predicate.js'

export const inputValue = /*#__PURE__*/ path (['target', 'value'])

/*
 * Usage:
 *   ; ['css-selector', 'css-selector', ...] | hideAllChildrenOf (element)
 *
 *   Note: uses NodeList.forEach and hence needs a polyfill for some browsers.
 */

export const hideAllChildrenOf = /*#__PURE__*/ el => each (
  sel => el.querySelectorAll (sel) | each (
    x => x.style.display = 'none'
  )
)

/*
 * Usage:
 *   // --- these are deprecated (keypress should no longer be used, see MDN)
 *   onKeyPress={keyPressListen (onSubmit, 'Enter')}
 *   onKeyPress={keyPressListenPreventDefault (onSubmit, 'Enter')}
 *
 *   // --- `f` receives the event as its only argument. Note also that the order is switched
 *   relative to the old `keyPressListen` function.
 *   const onKeyDown = useCallback (keyDownListen ('Enter', onSubmit), [...])
 *   // or to prevent default
 *   // const onKeyDown = useCallback (keyDownListenPreventDefault ('Enter', onSubmit), [...])
 *   // or to help the linter check the dependencies for useCallback
 *   // const onKeyDown = useCallback ((event) => event | keyDownListenPreventDefault ('Enter', onSubmit), [...])
 *   onKeyDown={keyDownListen ('Enter', onSubmit)}
 *   onKeyDown={keyDownListenPreventDefault ('Enter', onSubmit)}
 */

// --- @deprecated
export const keyPressListen = /*#__PURE__*/ recurry (3) (f => keyName => prop ('key') >> whenEquals (keyName) (f))
// --- @deprecated
export const keyPressListenPreventDefault = /*#__PURE__*/ f => keyName => (event) =>
  event | prop ('key') | whenEquals (keyName) (() => event
    | preventDefault
    | f
  )

const _keyDownListen = /*#__PURE__*/ recurry (4) (
  (preventDefault) => (keyName) => (f) => (event) => {
	if (event.key !== keyName) return
	if (preventDefault) event.preventDefault ()
	return f (event)
  },
)

export const keyDownListen = /*#__PURE__*/ _keyDownListen (false)
export const keyDownListenPreventDefault = /*#__PURE__*/ _keyDownListen (true)

/*
 * Usage:
 *   ref | eachSelector ('img') ((node) => ...)
 */

export const eachSelector = /*#__PURE__*/ sel => f => dot1 ('querySelectorAll') (sel) >> each (f)

export const onEventNativeWithAfter = /*#__PURE__*/ recurry (4) ((after) => (eventString) => (f) => (elem) => {
  const g = event => f (event, elem) | tap ((ret) => after (event, elem, ret))
  return elem | addEventListener (eventString, g)
})

export const onEventNative = /*#__PURE__*/ noop | onEventNativeWithAfter

export const onEventNativeStopPropagation = /*#__PURE__*/ onEventNativeWithAfter (
  (event, ..._) => event | stopPropagation
)

export const onClickNative = /*#__PURE__*/ 'click' | onEventNative
export const onClickNativeStopPropagation = /*#__PURE__*/ 'click' | onEventNativeStopPropagation

/*
 * Side-effect version of 'elem.classList.add (...)'
 * Note: might need polyfill for older browsers; IE10 & 11 should be ok.
 */

export const addClass = /*#__PURE__*/ cls => tap (prop ('classList') >> dot1 ('add') (cls))

const removeBooleans = lets (
  () => (x) => x === true || x === false,
  (f) => filter (not << f),
)

/* Filters literal booleans from a list and joins it on space.
 * Useful for building a list of classes for a component with conditionals:
 *   clss (visible && '.x--visible', highlight || '.x--normal', class1, class2, ...)
*/
export const clss = /*#__PURE__*/ list >> removeBooleans >> join (' ')

export const addEventListener = /*#__PURE__*/ side2 ('addEventListener')
export const stopPropagation = /*#__PURE__*/ side ('stopPropagation')
export const preventDefault = /*#__PURE__*/ side ('preventDefault')

export const querySelector = /*#__PURE__*/ dot1 ('querySelector')
export const scrollIntoView = /*#__PURE__*/ recurry (2) (
  options => node => scrollIntoViewMod (node, options)
)
