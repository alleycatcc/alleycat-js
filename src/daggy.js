import {
  pipe, composeRight, compose,
  dot,
} from 'stick-js/es'

export const dag = /*#__PURE__*/ type => x => type.is (x)
export const toJS = /*#__PURE__*/ dot ('toJS')
