; `
Usage:

Put something like this in a common location and design your breakpoints as you wish:

export const mediaPhone      = /*#__PURE__*/ 0    | mgt | mediaRule
export const mediaPhoneBig   = /*#__PURE__*/ 576  | mgt | mediaRule
export const mediaTablet     = /*#__PURE__*/ 768  | mgt | mediaRule
export const mediaDesktop    = /*#__PURE__*/ 992  | mgt | mediaRule
export const mediaDesktopBig = /*#__PURE__*/ 1200 | mgt | mediaRule

Then in the component:

  \${mediaQuery (
    mediaPhone (\`
      color: yellow;
      font-size: 20px;
    \`),
    mediaTablet (\`
      color: green;
      font-size: 15px;
    \`),
  )}

If you need the props:

  \${props => mediaQuery (
    mediaPhone (\`
      color: yellow;
      font-size: 20px;
    \`),
    mediaTablet (\`
      color: green;
      font-size: 15px;
    \`),
  )}
`

import {
  pipe, compose, composeRight,
  sprintfN, join, map,
} from 'stick-js'

;`
  mediaRule := mediaClause AND widthClauses { css }
  mediaClause := '@media only screen'
  widthClauses := widthClause (AND widthClause ...)
  widthClause := (min-width: n) | (max-width: n)
`

// --- [comp, val] -> String
const widthRule = sprintfN ('(%s: %spx)')
const widthClauses = map (widthRule) >> join (' and ')

const mediaCondition = /*#__PURE__*/ (spec) => [
  '@media all',
  spec | widthClauses,
] | join (' and ')

export const mlt = /*#__PURE__*/ x => [['max-width', x]]
export const mgt = /*#__PURE__*/ x => [['min-width', x]]
export const mwithin = /*#__PURE__*/ ([x, y]) => [['min-width', x], ['max-width', y]]

export const mediaRule = /*#__PURE__*/ (spec) => (css) => [mediaCondition (spec), css] | sprintfN ('%s {%s}')
export const mediaQuery = /*#__PURE__*/ (...lines) => lines | join ('\n')

// --- @deprecated
export const mediaComp = /*#__PURE__*/ (...css) => (spec) => mediaRule (spec) (css | join ('\n'))
export const media = /*#__PURE__*/ (...lines) => lines | join ('\n')
