import {
  pipe, compose, composeRight,
  lets, sprintf1,
  deconstruct2,
  isFunction, isString, isNumber,
} from 'stick-js/es'

import React from 'react'
const { useEffect, useContext, } = React

// --- @peer
import { ReactReduxContext, } from 'react-redux'

import invariant from 'invariant'

import { isEmptyString, conformsTo, } from '../general.js'
import { checkStore, } from '../redux.js'

export const modeDaemon = /*#__PURE__*/ 0
export const modeOnceTillUnmount = /*#__PURE__*/ 1
export const modeRestartOnRemount = /*#__PURE__*/ 2

/*
 * modeRestartOnRemount: saga started component mount and cancelled (with `task.cancel ()`) on unmount. (default).
 * modeDaemon:           saga starts on component mount and never cancels or restarts.
 * modeOnceTillUnmount:  saga cancels on unmount and never starts again.
 */

const allowedModes = [modeRestartOnRemount, modeDaemon, modeOnceTillUnmount]

const checkKey = /*#__PURE__*/ (key) => invariant (
  isString (key) && !isEmptyString (key),
  'injectSaga: Expected `key` to be a non empty string',
)

const shape = {
  saga: isFunction,
  mode: (mode) => isNumber (mode) && allowedModes.includes (mode),
}

const checkDescriptor = /*#__PURE__*/ (descriptor) => invariant (
  descriptor | conformsTo (shape),
  'injectSaga: Expected a valid saga descriptor, got: ' + JSON.stringify (descriptor),
)

const injectSagaFactory = /*#__PURE__*/ (store, isValid) =>
  (key, descriptor = {}, args) => {
    if (!isValid) checkStore (store)

    const newDescriptor = {
      ...descriptor,
      mode: descriptor.mode || modeRestartOnRemount,
    }
    const { saga, mode, } = newDescriptor

    checkKey (key)
    checkDescriptor (newDescriptor)

    let hasSaga = Reflect.has (store.injectedSagas, key)

    if (process.env.NODE_ENV !== 'production') {
      const oldDescriptor = store.injectedSagas [key]
      // --- rb: enable hot reloading of daemon and once-till-unmount sagas
      if (hasSaga && oldDescriptor.saga !== saga) {
        oldDescriptor.task.cancel ()
        hasSaga = false
      }
    }

    if (!hasSaga || (hasSaga && mode !== modeDaemon && mode !== modeOnceTillUnmount))
      store.injectedSagas[key] = {
        ...newDescriptor,
        task: store.runSaga (saga, args),
      }
}

const ejectSagaFactory = /*#__PURE__*/ (store, isValid) => (key) => {
  if (!isValid) checkStore (store)

  checkKey (key)

  const { mode, task, } = store.injectedSagas [key] || {}

  if (!mode || mode === modeDaemon) return

  task.cancel ()

  // --- rb: only clean up in production b/c/ in development we need `descriptor.saga` for hot
  // reloading; 'done' because we need some value to be able to detect `modeOnceTillUnmount` sagas in
  // `injectSaga`

  if (process.env.NODE_ENV === 'production')
    store.injectedSagas [key] = 'done'
}

const getInjectors = /*#__PURE__*/ (store) => {
  // --- @todo why do we need to check the store again here?
  checkStore (store)

  return {
    injectSaga: injectSagaFactory (store, true),
    ejectSaga: ejectSagaFactory (store, true),
  }
}

export const useSaga = /*#__PURE__*/ ({ key, saga, mode, }) => {
  const store = useContext (ReactReduxContext).store
  useEffect (() => {
    const injectors = getInjectors (store)
    injectors.injectSaga (key, { saga, mode, })

    return () => injectors.ejectSaga (key)
  }, [])
}
