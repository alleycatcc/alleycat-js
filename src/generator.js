import {
  pipe, composeRight, compose,
  dot, prop,
} from 'stick-js/es'

export const next = /*#__PURE__*/ dot ('next')
export const done = /*#__PURE__*/ prop ('done')
export const value = /*#__PURE__*/ prop ('value')
