import {
  pipe, compose, composeRight,
  id, noop, divideBy, always, map, addIndex, ok, whenOk, dot, dot1,
  ifPredicate, whenPredicate, eq, lt, gt, ifFalse, ifTrue, concat,
  condS, cond, otherwise, guard, sprintf1, lets, blush,
  prop, dot2, exception, raise, invoke, defaultTo, ne, ifOk,
  whenTrue, join, T, F, not, tap, list, path, prepend, whenNotOk,
  assoc, notOk, toThe, isObject, isArray, mergeToM, reduce,
  letS, assocM, concatTo, assocPathM, split, againstAll, find,
  bindProp, recurry, head, side,
  flip, concatM,
  mapTuples,
  fromPairs,
  deconstruct,
  allAgainst, anyAgainst,
  appendM, lte, ifYes,
  merge, values, reduceRightC,
  ampersandN,
  remapTuples, tryCatch, has, updateM,
} from 'stick-js/es'

import { Just, Nothing, Left, Right, } from './bilby.js'
import { ifArray, ifHasN, ifEmptyString, } from './predicate.js'

// @stick
export const composeManyRight = /*#__PURE__*/ (first, ...args) => reduce (
  (f, acc) => f >> acc,
  first,
  args
)

// --- endpoints inclusive.
export const between = /*#__PURE__*/ recurry (3) (m => n => x => x >= m && x <= n)

export const reverseM = /*#__PURE__*/ dot ('reverse')
export const reverse = /*#__PURE__*/ xs => xs.reduceRight ((acc, x) => (acc.push (x), acc), [])

export const iwarn  = /*#__PURE__*/ (...args) => console.warn ('Internal warning:', ...args)
export const ierror = /*#__PURE__*/ (...args) => console.error ('Internal error:', ...args)
export const info   = /*#__PURE__*/ (...args) => console.info (...args)
export const warn   = /*#__PURE__*/ (...args) => console.warn (...args)
export const error  = /*#__PURE__*/ (...args) => console.error (...args)
export const debug  = /*#__PURE__*/ (...args) => console.debug (...args)

export const mapX = /*#__PURE__*/ map | addIndex
export const trim = /*#__PURE__*/ dot ('trim')
export const length = /*#__PURE__*/ prop ('length')
export const slice = /*#__PURE__*/ dot2 ('slice')
export const slice1 = /*#__PURE__*/ dot1 ('slice')

// --- @deprecated (moved to predicate.js)
export const isEmptyList = /*#__PURE__*/ prop ('length') >> eq (0)
export const isNotEmptyList = /*#__PURE__*/ isEmptyList >> not
export const isEmptyString = /*#__PURE__*/ isEmptyList
export const isWhiteSpaceOrEmpty = /*#__PURE__*/ trim >> isEmptyString
//

export const tellIf = /*#__PURE__*/ ifFalse (
  _ => noop,
  _ => (...args) => console.log (...args),
)

export const resolveP = /*#__PURE__*/ (...args) => {
  console.log ('alleycat-js/general/resolveP is deprecated, please use alleycat-js/async/resolveP')
  return Promise.resolve (...args)
}
export const rejectP  = /*#__PURE__*/ (...args) => {
  console.log ('alleycat-js/general/rejectP is deprecated, please use alleycat-js/async/rejectP')
  return Promise.reject (...args)
}
export const allP     = /*#__PURE__*/ (...args) => {
  console.log ('alleycat-js/general/allP is deprecated, please use alleycat-js/async/allP')
  return Promise.all (...args)
}
export const startP   = /*#__PURE__*/ _ => {
  console.log ('alleycat-js/general/startP is deprecated, please use alleycat-js/async/startP')
  return null | resolveP
}

// --- Number -> String -> String
export const prettyBytes = /*#__PURE__*/ invoke (() => {
  const row = fmt => (pred, n, suffix) =>
    1024 | toThe (n + 1) | pred | guard (
      divideBy (1024 | toThe (n)) >> sprintf1 (fmt) >> concat (' ' + suffix)
    )

  return numDecimals => lets (
    ()          => numDecimals | sprintf1 ('%%.%sf'),
    (fmt)       => fmt | row,
    (_, rowFmt) => condS ([
        rowFmt (lt, 0, 'b'),
        rowFmt (lt, 1, 'k'),
        rowFmt (lt, 2, 'M'),
        rowFmt (lt, 3, 'G'),
        rowFmt (_ => T, 3, 'G'),
    ]),
  )
})

export const isTrue = /*#__PURE__*/ eq (true)
export const isFalse = /*#__PURE__*/ eq (false)

// @test
export const allUniqueAndOkAnd = /*#__PURE__*/ (pred) => (xs) => {
  const seen = new Map
  const notPred = pred >> not
  for (const x of xs) {
    if (x | notPred) return false
    if (x | notOk) return false
    if (seen.has (x)) return false
    seen.set (x, true)
  }
  return true
}

export const allUniqueAndOk = /*#__PURE__*/ allUniqueAndOkAnd (T)

export const compareDates = /*#__PURE__*/ (a, b) => lets (
  _ => a | Number,
  _ => b | Number,
  (an, bn) => an === bn ? 0 : an < bn ? -1 : 1
)

/*

test allUniqueAndOk

; [
    [void 8, void 8],
    [void 8, null],
    [void 8, 3],
    [1, 1],
    [1, 2],
    ['1', 1],
    [null, 1],
    [null, null],
    [null, void 8],
    [1, 1, 1, 1, 1],
    [1, 1, 1, 1, 2],
    [1, 2, 3, 4, 5],
]
*/

export const checkUploadFilesLength = /*#__PURE__*/ (files, alertFunc = noop) => files.length | condS ([
  0 | eq | guard (_ => ierror ('empty file list')),
  1 | gt | guard (_ => 'Too many files'
    | tap (alertFunc)
    | F
  ),
  otherwise | guard (T),
])

export const checkUploadFilesSize = /*#__PURE__*/ ({
  file,
  maxFileSize: max = 1024 * 1024,
  prettyBytesDecimalPlaces: places = 0,
  alertFunc = noop,
}) => file.size | condS ([
  max | gt | guard (_ => max
    | prettyBytes (places)
    | sprintf1 ('File too large! (max = %s)')
    | tap (alertFunc)
    | F
  ),
  otherwise | guard (T),
])

// export const nullToUndef = /*#__PURE__*/ ifNull (noop) (id)

export const color = /*#__PURE__*/ x => ['#', x] | join ('')

// --- android webview only shows the first arg by default; note that logging objects won't work
// without an inspect () call.
export const logAndroid = /*#__PURE__*/ list >> join (',') >> console.log

export const logAndroidPerf = /*#__PURE__*/ (...args) => logAndroid (...(args | prepend (performance.now ())))

export const log = /*#__PURE__*/ console | bindProp ('log')
export const logWith = /*#__PURE__*/ (header) => (...args) => log (... [header, ...args])
export const infoWith = /*#__PURE__*/ (header) => (...args) => info (... [header, ...args])

export const divMod = /*#__PURE__*/ m => n => lets (
  _ => n % m,
  (mod) => (n - mod) / m,
  (mod, div) => [div, mod],
)

export const andN = /*#__PURE__*/ (...args) => {
  for (const i of args)
    if (!i) return false
  return true
}

export const defaultToV = /*#__PURE__*/ recurry (2) (
  (v) => defaultTo (v | always),
)

export const mergeAll = /*#__PURE__*/ xs => xs | reduce ((tgt, src) => mergeToM (tgt, src), {})

export const singletonArray = /*#__PURE__*/ ifArray (id, x => [x])

/*
export const addCommas = #__PURE__ invoke (() => {
  const ifLengthUnder4 = ifPredicate (length >> lt (4))
  const comma = ifLengthUnder4 (
    id,
    letS ([
      splitAt (3),
      (_, [b, c]) => [b, c | comma] | join (','),
    ]),
  )
  return String >> reverse >> comma >> reverse
})
*/

const { hasOwnProperty: hasOwn, } = {}

const _reduceObjDeep = /*#__PURE__*/ (f) => (acc) => (path) => (o) => {
  let curAcc = acc
  for (const k in o) if (hasOwn.call (o, k)) {
    path.push (k)
    const v = o [k]
    // --- also arrays
    if (typeof v === 'object') curAcc = _reduceObjDeep (f) (acc) (path) (v)
    else curAcc = f (curAcc, [k, v, path])
    path.pop ()
  }
  return curAcc
}

// xxx stick
export const reduceObjDeep = /*#__PURE__*/ (f) => (acc) => (o) => _reduceObjDeep (f) (acc) ([]) (o)

export const getQueryParams = /*#__PURE__*/ () => document.location.search
  | defaultToV ('?')
  | slice1 (1)
  | ifEmptyString (
    () => ({}),
    split ('&') >> map (split ('=') >> deconstruct (
      ([k, v]) => [k, v | decodeURIComponent],
    )) >> fromPairs,
  )

export const getQueryParam = /*#__PURE__*/ key => getQueryParams () | prop (key)

// --- be careful, toString is also a built-in function and forgetting to import it can lead to an
// annoying bug.
export const toString = /*#__PURE__*/ dot ('toString')
export const toString1 = /*#__PURE__*/ dot1 ('toString')

// --- functions which have been useful at some point or other.
const okAndNotFalse = /*#__PURE__*/ againstAll ([ok, false | ne])
const ifOkAndNotFalse = /*#__PURE__*/ okAndNotFalse | ifPredicate

; `

Usage:
  const threeAtATime = 3 | nAtATime
  ; [1, 2, 3, 4, 5, 6] | threeAtATime // => [[1, 2, 3], [4, 5, 6]]

Note: there are two versions now, the second one does not discard the chunk at the end.
`
const __nAtATime = /*#__PURE__*/ (keepChunk) => (n) => (xs) => {
  const ret = []
  let i = -1, j = 0
  let cur = [], curlen = 0
  const len = xs | length
  for (let j = 0; j < len; j++) {
    const x = xs [j]
    curlen += 1
    cur.push (x)
    if ((curlen === n) || (keepChunk && j === len-1)) {
      ret.push (cur)
      cur = [], curlen = 0
    }
  }
  return ret
}

export const nAtATime = /*#__PURE__*/ __nAtATime (false)
export const nAtATimePlusChunk = /*#__PURE__*/ __nAtATime (true)

export const roundUp = /*#__PURE__*/ letS ([
  Math.floor,
  (x, floor) => x === floor ? x : floor + 1,
])

export const roundDown = /*#__PURE__*/ Math.floor

// --- babel bug prevents one-liner
const { round, } = Math
export { round, }

// @todo
// e.g. argTuple (0, 2) -> (x, _, y) -> [x, y]
// const argTuple

export const min = /*#__PURE__*/ recurry (2) (a => b => Math.min (a, b))
export const max = /*#__PURE__*/ recurry (2) (a => b => Math.max (a, b))

export const notBelow = /*#__PURE__*/ max
export const notAbove = /*#__PURE__*/ min

// --- @todo move to stick
// --- @todo check currying
// @check only necessary in order to ensure that the result has a well-defined arity, vs. rest args

// --- This is a version of `addIndex` for a functions with arity 2, e.g. `reduce`.
export const addIndex2 = /*#__PURE__*/ (orig) => (f) => (x) => (coll) => {
    let idx = -1
    const g = (...args) => f (...args, ++idx)
    return orig (g) (x) (coll)
}

export const reduceX = /*#__PURE__*/ reduce | addIndex2

; `
Replacement for URLSearchParams, which causes problems on IE, even with polyfill.

Usage:
  fetch ({
    ...
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    },
    body: { a, b, c, } | encodeFormData,
  })

`

export const encodeFormData = /*#__PURE__*/ remapTuples (list >> map (encodeURIComponent) >> join ('=')) >> join ('&')

// --- @todo zip functions to stick

// --- note, not curried.
export const zipWithAll = /*#__PURE__*/ (f, ...xss) => {
  const ret = []
  const n = Math.min (...xss.map (xs => xs.length))
  for (let i = 0; i < n; i++) ret.push (
    f (...xss.map (xs => xs [i])),
  )
  return ret
}

export const zipWithN = /*#__PURE__*/ recurry (2) (
  f => xs => zipWithAll (f, ...xs),
)

export const zipWith3 = /*#__PURE__*/ recurry (4) (
  (f) => (xs) => (ys) => (zs) => zipWithAll (f, xs, ys, zs),
)

export const zip = /*#__PURE__*/ (...xss) => xss | zipWithN (list)

export const partition = /*#__PURE__*/ recurry (2) (
  p => flip (reduce) (
    [[], []],
    ([left, right], x) => p (x)
      ? [left | appendM (x), right]
      : [left, right | appendM (x)],
  ),
)

// --- old-fashioned way to 'subclass' a class, in this case to make a custom throwable Error.
export function PromiseCancellation () {}
PromiseCancellation.prototype = Object.create (Error.prototype)

export const headMaybe = /*#__PURE__*/ xs => xs.length === 0 ? Nothing : xs | head | Just

export const toHex = /*#__PURE__*/ toString1 (16) >> concatTo ('0x')
export const curlyQuote = /*#__PURE__*/ sprintf1 ('“%s”')

export const pathDot = /*#__PURE__*/ split ('.') >> path

// --- @todo move to stick
; `immutable pop`
export const pop = /*#__PURE__*/ (xs) => [...xs] | side ('pop')

export const setTimeoutOn = /*#__PURE__*/ recurry (2) (
  f => ms => setTimeout (f, ms)
)

export const setTimeoutTo = /*#__PURE__*/ setTimeoutOn | flip

export const setIntervalOn = /*#__PURE__*/ recurry (2) (
  f => ms => setInterval (f, ms)
)

export const setIntervalTo = /*#__PURE__*/ setIntervalOn | flip

export const eqZip = /*#__PURE__*/ ys => xs => zipWithAll (eq) (xs, ys) | allAgainst (Boolean)

export const pluckN = /*#__PURE__*/ recurry (2) ((ps) => (o) => {
  const ret = {}
  for (const p of ps) ret [p] = o [p]
  return ret
})

// @experimental same as prop then?
export const pluck = /*#__PURE__*/ recurry (2) (p => o => pluckN ([p]) (o))

// @experimental only manually curried
export const pluckAll = /*#__PURE__*/ (...xs) => o => pluckN (xs) (o)

export const repluckN = /*#__PURE__*/ recurry (2) ((ps) => (o) => {
  const ret = []
  for (const p of ps) ret.push (o [p])
  return ret
})

export const repluck = /*#__PURE__*/ recurry (2) (p => o => repluckN ([p]) (o))
// @experimental only manually curried
export const repluckAll = /*#__PURE__*/ (...xs) => o => repluckN (xs) (o)

export const intersperse = /*#__PURE__*/ (f) => (xs) => {
  const l = xs.length
  if (l === 0 || l === 1) return xs
  const [y, ...ys] = xs
  return ys | flip (reduce) (
    [y],
    (acc, x) => acc | concatM ([f (), x]),
  )
}

export const addIndexF = /*#__PURE__*/ (idxF) => (orig) => (f) => {
    let idx = -1
    const g = (...args) => f (...args, ++idx | idxF)
    return orig (g)
}

/*
const odd = x => x % 2 === 1
; [1, 2, 3, 4, 5, 6, 5, 4, 3, 2, 1] | partition (T) | console.log
; [1, 2, 3, 4, 5, 6, 5, 4, 3, 2, 1] | partition (F) | console.log
; [1, 2, 3, 4, 5, 6, 5, 4, 3, 2, 1] | partition (odd) | console.log
*/

export const truncate = /*#__PURE__*/ recurry (2) (
  l => letS ([
    length >> lte (l) >> ifYes (
      () => id,
      () => slice (0, l) >> concat (' …'),
    ),
    (s, f) => s | f,
  ]),
)

// --- given an object and a list of keys, remove those keys and return a list of the remaining
// values.
// --- the order of the output list is the order returned by `values`.

export const valuesWithMask = /*#__PURE__*/ recurry (2) (
  ignore => o => lets (
    () => ignore | reduceRightC (x => assocM (x, null), {}),
    (m) => o | merge (m) | values,
  ),
)

// --- like a 'reverse' map, hence the same: given an array of functions and a value, apply each
// function to the value.
// --- see stick-js for caveat about the symbol `ampersandN`
// 3 | pam ([inc, double, odd]) // => [4, 6, true]

export const pam = /*#__PURE__*/ ampersandN

export const conformsTo = /*#__PURE__*/ (shape) => (o) => {
  for (const k in shape) {
    const p = shape [k]
    if (!p (o [k])) return false
  }
  return true
}

/* This function prepends a string to an 'error' value, defined loosely
 * as a promise rejection, a value picked up in a `catch` block, the left
 * side of an Either, and so on.
 *
 * It's generally hard to make an assumption about whether or not it's an
 * exception object, so we use some heuristics to make this work
 * transparently: we check if the value has a `.stack` and a `.message`
 * property. If so, we prefix a string to each property and return the
 * object; otherwise, we turn the value into a string, prefix the value and
 * return the string.
 *
 * Idioms:
 *
 * doSomethingImportant ()
 * | then (...)
 * | recover (decorateRejection ('Unable to do something important: ') >> warn)
 *
 * or
 *
 * // --- where `die` means throw a new exception, which since we're in a
 * promise chain, means reject again.
 * | recover (decorateRejection ('Unable to do something important: ') >> die)
 *
 * or
 *
 * tryCatch (
 *   (res) => onSuccess (res),
 *   // --- decorate and rethrow
 *   decorateRejection ("Couldn't do something important: ") >> die,
 *   () => doSomethingImportant (),
 * )
 *
 * or
 *
 * doSomethingReturningEither ()
 * | fold (
 *   // --- repackage it as a new Left
 *   decorateRejection ('Unable to do something important: ') >> Left,
 *   (r) => onSuccess (r),
 * )
 */

export const decorateRejection = /*#__PURE__*/ recurry (2) (
  (msg) => (e) => (e ?? '') | ifHasN (['stack', 'message']) (
    ([... _], o) => o
      | updateM ('stack', concatTo (msg))
      | updateM ('message', concatTo (msg)),
    (o) => o | toString | concatTo (msg),
  ),
)

// --- `errorMsgFunc` takes no arguments and returns a string.
export const tryAsEither = /*#__PURE__*/ recurry (2) (
  (errorMsgFunc) => (f) => tryCatch (
    Right,
    decorateRejection (errorMsgFunc ()) >> Left,
    f,
  ),
)

// export const toSet = /*#__PURE__*/ xs => new Set (xs)
// export const setAddM = /*#__PURE__*/ x => s => (s.add (x), s)
// export const setAddToM = /*#__PURE__*/ s => x => (s.add (x), s)
// export const setHas = /*#__PURE__*/ bindProp ('has')
// export const ifSetHas = /*#__PURE__*/ setHas >> ifPredicate
