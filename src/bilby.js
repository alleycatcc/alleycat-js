// --- bilby.js has no alleycat-js dependencies.

import {
  pipe, composeRight, compose,
  dot1, dot2, dot3, dot4,
  ifOk, always, id, prop,
  head, tail, bindPropTo,
  die, concatTo, not,
  recurry, ifPredicate,
} from 'stick-js/es'

import bilby from 'bilby'
const { left: Left, right: Right, some: Just, none: Nothing, } = bilby
export { Left, Right, Just, Nothing, }

export const isLeft = /*#__PURE__*/ prop ('isLeft')
export const fold = /*#__PURE__*/ dot2 ('fold')
export const fold3 = /*#__PURE__*/ dot3 ('fold')
export const fold4 = /*#__PURE__*/ dot4 ('fold')
export const flatMap = /*#__PURE__*/ dot1 ('flatMap')

export const toEither = /*#__PURE__*/ l => ifOk (
  Right,
  l | Left | always,
)

export const toMaybe = /*#__PURE__*/ ifOk (Just, Nothing | always)

export const isJust = /*#__PURE__*/ prop ('isSome')
export const isNothing = /*#__PURE__*/ isJust >> not
export const ifJust = /*#__PURE__*/ isJust | ifPredicate
export const ifNothing = /*#__PURE__*/ isNothing | ifPredicate

// --- @deprecated, use `foldWhenJust`
export const foldJust = /*#__PURE__*/ f => fold (
  f, void 8,
)

// :: (a -> b) -> Maybe a -> b | undefined
export const foldWhenJust = /*#__PURE__*/ recurry (2) (
  (f) => (mb) => mb | fold (f, void 8),
)

export const toJust = /*#__PURE__*/ fold (id, void 8)

// --- the way fold works for Maybe in bilby is unfortunate -- the Nothing case only wants a value,
// not a function. To make it work with a function use `foldMaybe`.
// --- @future deprecate `fold` and rename `foldMaybe` to `fold`
export const foldMaybe = /*#__PURE__*/ recurry (3) (
  (just) => (nothing) => ifJust (
    just << toJust,
    () => nothing (),
  ),
)

const colon = /*#__PURE__*/ (x, xs) => [x, ...xs]
const bilbyLiftA2 = /*#__PURE__*/ 'liftA2' | bindPropTo (bilby)

// --- note that `f` must be curried (manual is ok).
export const liftA2 = /*#__PURE__*/ recurry (3) (
  f => x => y => bilbyLiftA2 (f, x, y)
)

// --- note that `f` must be curried (manual is ok).
export const liftA2N = /*#__PURE__*/ recurry (2) (
  f => xs => bilbyLiftA2 (f, ...xs)
)

// @test
export const sequenceM = /*#__PURE__*/ (pure) => {
  const _sequence = xs => xs.length === 0
    ? ([] | pure)
    : bilbyLiftA2 (colon, xs | head, xs | tail | _sequence)
  return _sequence
}

export const cata = /*#__PURE__*/ dot1 ('cata')

export const foldRight = /*#__PURE__*/ decorate => fold (
  l => l | concatTo (decorate + ' ') | die,
  id,
)
