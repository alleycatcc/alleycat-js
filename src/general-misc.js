import {
  pipe, composeRight, compose,
  prop, map, addIndex,
  always, id,
  not, T, F, join,
  ok, dot1, dot2, side2,
  guard, otherwise,
  lets,
  concat, condS, guardV,
  againstEither,
  timesV,
  tryCatch,
  path,
  split,
  isType,
  ifPredicate,
  head,
  recurry,
} from 'stick-js/es'

// --- predicate has no alleycat-js dependencies.
import { ifLongerThan, ifNegativeOne, ifType, } from './predicate.js'
// --- bilby has no alleycat-js dependencies.
import { isJust, toJust, Nothing, Just, Left, Right, } from './bilby.js'

const removeSpaces = /*#__PURE__*/ dot2 ('replace') (/\s+/g) ('')

// --- beware, overwrites any flags that the re already had.
export const xRegExpFlags = /*#__PURE__*/ (re, flags) => new RegExp (
  re.source | removeSpaces,
  flags,
)

export const findPredOk = /*#__PURE__*/ recurry (2) (
  (pred) => (xs) => {
    for (const x of xs) {
      const p = pred (x)
      if (ok (p)) return p
    }
  }
)

export const findPredOkGen = /*#__PURE__*/ recurry (2) (
  (pred) => (gen) => {
    let n
    while (! (n = gen.next ()).done) {
      const p = pred (n.value)
      if (ok (p)) return p
    }
  }
)

export const findPredMaybeGen = /*#__PURE__*/ recurry (2) (
  (pred) => (gen) => {
    let n
    while (! (n = gen.next ()).done) {
      const p = pred (n.value)
      if (p | isJust) return p | toJust
    }
  }
)

export const tryCatchS = /*#__PURE__*/ recurry (4) (
  good => bad => f => v => tryCatch (good, bad, _ => f (v))
)

// ------ lazyfish extensions
export const lazyFindPred = /*#__PURE__*/ recurry (2) ((pred, lxs) => {
  while (true) {
    const { value, done, } = lxs.next ()
    if (done) break
    const predVal = pred (value)
    if (predVal) return predVal
  }
})

export const substring = /*#__PURE__*/ dot2 ('substring')

export const ellipsisAfter = /*#__PURE__*/ recurry (2) (
  n => s => s | ifLongerThan (n) (
    substring (0, n) >> concat ('…'),
    id,
  )
)

// --- doesn't truncate if too long.
export const padTo = /*#__PURE__*/ recurry (2) (
  n => str => lets (
    _ => str.length,
    (l) => str | condS ([
      (_ => l >= n) | guardV (str),
      otherwise | guard (x => x | (timesV (n - l) (' ') | join ('') | concat)),
    ])
  )
)

export const on = /*#__PURE__*/ side2 ('on')

export const uniqueWith = /*#__PURE__*/ (f) => (xs) => {
  const ret = []
  const s = new Set
  for (const x of xs) {
    const xx = f (x)
    if (s.has (xx)) continue
    ret.push (x)
    s.add (f (x))
  }
  return ret
}

export const errString = /*#__PURE__*/ prop ('message')
export const errFull = /*#__PURE__*/ ifType ('Error') (
  prop ('stack'),
  id,
)
