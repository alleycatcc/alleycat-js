import {
  pipe, compose, composeRight,
  always, id,
  tap, condS, guard, otherwise, ne,
  noop, lets, ok,
  sprintf1, sprintfN,
  isArray, ifPredicate, ifOk,
  invoke, recurry,
  isFunction, isObject,
  modulo, eq, reduceRightC, join,
  die, mergeM, list,
} from 'stick-js/es'

import invariant from 'invariant'

import { log, logWith, composeManyRight, conformsTo, toString, nAtATime, } from './general.js'

const ifArray = /*#__PURE__*/ isArray | ifPredicate

export const action = /*#__PURE__*/ (f=noop, tag=null) => lets (
  () => Symbol (),
  (type) => (...args) => ({
    tag, // --- for debugging
    type,
    data: f (...args),
  }),
  (type, action) => {
    action.c = type
    return action
  },
)

export const makeReducer = /*#__PURE__*/ invoke (() => {
  const isOdd = modulo (2) >> eq (1)
  // --- right reduce using n-sized chunks.
  const reduceNRightC = recurry (3) (
    (n) => (f) => (acc) => nAtATime (n) >> reduceRightC (f, acc)
  )
  return (...args) => {
    const l = args.length
    if (l | isOdd) die ('makeReducer: need even # of args')
    const ret = {}
    return args | reduceNRightC (2) (
      ([action, f]) => mergeM ({
        [action.c]: ({ data, }) => f (data),
      }),
      {},
    )
  }
})

/*
 * Usage:
 *
 *   const reducerTable = {
 *     // --- constant to listen for (i.e. `type` of the action payload).
 *     [constant1]:
 *       (actionPayload) => (curState) =>
 *         // --- the new state -- be sure it's an immutable update.
 *         <new-state>,
 *
 *     // --- the updater can also be a list of immutable update operations, in which case they'll
 *     be composed in order:
 *
 *     [constant2]: (actionPayload) => [update1, update2, ...],
 *
 *     ...
 *   }
 *
 * Example:
 *
 *   import { update, merge, plus, } from 'stick-js'
 *
 *   const initialState = {
 *     curTime: 0,
 *     ...
 *   }
 *
 *   const reducerTable = {
 *     [TIMER_ADVANCE]: ({ data: millis, }) => (curState) => ({
 *       ...curState,
 *       curTime: curState.curTime + millis,
 *     }),
 *
 *     // --- or
 *     [TIMER_ADVANCE]: ({ data: millis, }) => update (
 *       'curTime',
 *       plus (millis),
 *     }),
 *
 *     [TIMER_SET]: ({ data: millis, }) => (curState) => ({
 *       ...curState,
 *       curTime: millis,
 *     }),
 *
 *     // --- or
 *     [TIMER_SET]: ({ data: millis, }) => merge ({
 *       curTime: millis,
 *     }),
 *   }
 *
 */

export const checkStore = /*#__PURE__*/ invoke (() => {
  const shape = {
    dispatch: isFunction,
    subscribe: isFunction,
    getState: isFunction,
    replaceReducer: isFunction,
    runSaga: isFunction,
    injectedReducers: isObject,
    injectedSagas: isObject,
  }

  return (store) => invariant (
    store | conformsTo (shape),
    '(app/utils...) injectors: Expected a valid redux store',
  )
})

export const reducerTell = /*#__PURE__*/ invoke (() => {
  const _reducer = reducerTable => action => {
    const updater = action | (reducerTable [action.type] || always (id))
    return updater | ifArray (
      (us) => composeManyRight (...us),
      id,
    )
  }

  const traceReducer = (reducerName, tellSpec, action) => tap (
    (state) => {
      if (!tellSpec) return
      if (tellSpec !== true && !tellSpec [reducerName])
        return
      const logAction = ({ tag, data, }) => lets (
        () => toString (tag || 'empty') | sprintf1 ('‘%s’'),
        () => data | ifOk (
          JSON.stringify >> sprintf1 ('with data: %s'),
          () => 'with no data',
        ),
        list >> join (' '),
      )
      const tag = reducerName | sprintf1 ('[reducer %s]\n→ ')
      const t = action.type
      if (!ok (t))
        return tag | sprintf1 ('%sunexpected: action without a type')
      // --- assumed to be one of our actions
      if (typeof t === 'symbol') return state | logWith (
        [tag, action | logAction] | sprintfN ('%sfired action %s\n→ new state ='),
      )
      // --- ignore internal redux actions
      if ((typeof t.match === 'function') && t.match (/^@@redux/)) return log (
        tag | sprintf1 ('%signoring internal @@redux action'),
      )
      // --- ignore other actions
      log (tag | sprintf1 ('%signoring unknown (non-alleycat) action'))
    }
  )

  return recurry (4) (
    (tellSpec) => (name) => (initialState) => (reducerTable) =>
      (state=initialState, action) => state
        | _reducer (reducerTable) (action)
        | traceReducer (name, tellSpec, action),
  )
})

export const reducer = /*#__PURE__*/ null | reducerTell
