import {
  pipe, compose, composeRight,
  remapTuples,
  invoke, lets, join,
  concatTo, assocM,
  assocPathM, recurry,
} from 'stick-js/es'

// --- @peer
import { defineMessages, } from 'react-intl'

import { reduceObjDeep, mergeAll, } from './general.js'

/*
 * @todo -- is this current? Check alleycat-frontend
 *
 * To be used with i18n.js.
 * (see gitlab:alleycatcc/passphrases-frontend)
 *
 * Example:
 *
 * ./app/translations/en.js:
 * ./app/translations/nl.js:
 * ./app/containers/GenerateTab/messages.js:
 * ./app/i18n.js
 */

const transformIntl = invoke (() => {
  const _transformIntl = idPref => (acc, [k, v, path]) => acc | lets (
    _ => path | join ('.'),
    concatTo (idPref + '.'),
    (_, key) => assocM (key) (v)
  )

  return (idPref, o) =>
    reduceObjDeep (idPref | _transformIntl) ({}) (o)
})

const transformIntlComponent = invoke (() => {
  const _transformIntlComponent = idPref => (acc, [k, v, path]) => acc | lets (
    _ => path | join ('.'),
    concatTo (idPref + '.'),
    (_, key) => assocPathM (path) ({
      id: key,
      defaultMessage: v,
    })
  )

  return recurry (2) ((idPref) => (o) =>
    reduceObjDeep (idPref | _transformIntlComponent) ({}) (o),
  )
})

// ------ for the messages files.
export const prepareIntl = /*#__PURE__*/ remapTuples (transformIntl) >> mergeAll

// ------ for the component files.
export const prepareIntlComponent = /*#__PURE__*/ recurry (2) (
  (idPref) => transformIntlComponent (idPref) >> defineMessages,
)
