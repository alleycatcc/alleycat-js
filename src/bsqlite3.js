import {
  pipe, compose, composeRight,
  bindProp, dot, dot1, dot2, map, tap,
  recurry, lets, sprintfN, id,
  join, spreadTo, head, list,
} from 'stick-js/es'

// --- @peer
import sqlite3 from 'better-sqlite3'

import daggy from 'daggy'

import { Left, Right, flatMap, cata, } from 'alleycat-js/es/bilby'
import { length, toString, tryAsEither, } from 'alleycat-js/es/general'
import { ifDefined, } from 'alleycat-js/es/predicate'

const Stmt = /*#__PURE__*/ daggy.taggedSum ('Stmt', {
  S: ['statement'],
  SB: ['statement', 'bindings'],
})

const { S, SB, } = Stmt
export { S, SB, }

S.prototype.toString = function () {
  return this | cata ({
    S: (statement) => statement,
    SB: (statement, _) => statement,
  })
}

export const getTransaction = /*#__PURE__*/ bindProp ('transaction')
export const getPrepare = /*#__PURE__*/ bindProp ('prepare')

const run = /*#__PURE__*/ dot ('run')
const run1 = /*#__PURE__*/ dot1 ('run')
const get = /*#__PURE__*/ dot ('get')
const get1 = /*#__PURE__*/ dot1 ('get')
const all = /*#__PURE__*/ dot ('all')
const all1 = /*#__PURE__*/ dot1 ('all')
const iterate = /*#__PURE__*/ dot ('iterate')
const iterate1 = /*#__PURE__*/ dot1 ('iterate')
const pragma = /*#__PURE__*/ dot2 ('pragma')

const getXHelper = /*#__PURE__*/ (db, stmts, wantTransaction) => [
  wantTransaction ? (db | getTransaction) : id,
  db | getPrepare,
  stmts | length,
]

const getX = /*#__PURE__*/ recurry (7) (
  (wantTransaction) => (stmtModifier) => (errF) => (noBindF) => (bindF) => (db) => (stmts) => lets (
    () => getXHelper (db, stmts, wantTransaction),
    ([transactionF, prepare, numStmts]) => tryAsEither (
      () => [numStmts, stmts | map (toString) | join ('\n')] | spreadTo (errF),
      () => stmts | transactionF (map (cata ({
        S: prepare >> stmtModifier >> noBindF,
        SB: (statement, bindings) => statement | prepare | stmtModifier | bindF (bindings),
      }))),
    ),
  ),
)

const withTransaction = /*#__PURE__*/ getX (true)
const withoutTransaction = /*#__PURE__*/ getX (false)

export const getRuns = /*#__PURE__*/ recurry (2) (
  (db) => (stmts) => withTransaction (
    id,
    list >> sprintfN ("Couldn't run %s statement(s): %s: "),
    run, run1, db, stmts,
  ),
)

export const getRun = /*#__PURE__*/ recurry (2) (
  (db) => (stmt) => getRuns (db, [stmt]) | map (head),
)

export const _getGet = /*#__PURE__*/ recurry (3) (
  (stmtModifier) => (db) => (stmt) => withoutTransaction (
    stmtModifier,
    (_, stmts) => [stmts] | sprintfN ("Couldn't get statement: %s: "),
    get, get1, db, [stmt],
  ) | map (head),
)

export const getGet = /*#__PURE__*/ _getGet (id)
export const getGetModify = /*#__PURE__*/ (stmtModifier) => _getGet (stmtModifier)

export const _getAll = /*#__PURE__*/ recurry (3) (
  (stmtModifier) => (db) => (stmt) => withoutTransaction (
    stmtModifier,
    (_, stmts) => [stmts] | sprintfN ("Couldn't get statement: %s: "),
    all, all1, db, [stmt],
  ) | map (head),
)

export const getAllModify = /*#__PURE__*/ (stmtModifier) => _getAll (stmtModifier)
export const getAll = /*#__PURE__*/ _getAll (id)

export const getIterate = /*#__PURE__*/ recurry (3) (
  (db) => (stmt) => (f) => withoutTransaction (
    id,
    (_, stmts) => [stmts] | sprintfN ("Couldn't iterate over statement: %s: "),
    iterate, iterate1, db, [stmt],
  ) | map (head >> f),
)

export const getPragmaWithOpts = /*#__PURE__*/ recurry (3) (
  (db) => (opts) => (pragmaString) => tryAsEither (
    () => "Couldn't run pragma: ",
    () => db | pragma (pragmaString, opts),
  ),
)

// --- arg can be used for pragmas which retrieve a value or ones which get a value (the parentheses
// syntax and the equals syntax mean the same thing).
export const getPragma1WithOpts = /*#__PURE__*/ recurry (4) (
  (db) => (opts) => (pragmaName) => (arg) => lets (
    // --- exactly equivalent to pragmaName = arg
    () => `${pragmaName}(${arg})`,
    (s) => getPragmaWithOpts (db, opts, s),
  ),
)

// --- `simple` = return first column of first row

export const getPragmaSimple = /*#__PURE__*/ recurry (2) (
  (db) => (pragmaString) => getPragmaWithOpts (
    db, { simple: true, }, pragmaString,
  ),
)

export const getPragma1Simple = /*#__PURE__*/ recurry (3) (
  (db) => (pragmaName) => (arg) => getPragma1WithOpts (
    db, { simple: true, }, pragmaName, arg,
  ),
)

export const getPragma = /*#__PURE__*/ recurry (2) (
  (db) => (pragmaString) => getPragmaWithOpts (
    db, {}, pragmaString,
  ),
)

export const getPragma1 = /*#__PURE__*/ recurry (3) (
  (db) => (pragmaName) => (arg) => getPragma1WithOpts (
    db, {}, pragmaName, arg,
  ),
)

export const expand = /*#__PURE__*/ dot1 ('expand')
export const raw = /*#__PURE__*/ dot1 ('raw')
export const pluck = /*#__PURE__*/ dot1 ('pluck')

// @todo implement .columns + a statement preparing function, with & without transactions.

export const getApi = /*#__PURE__*/ (...args) => lets (
  () => sqlite3 (...args),
  (db) => ({
    db,
    // --- automatically wrapped in a transaction
    runs: db | getRuns,
    run: db | getRun,
    all: db | getAll,
    allPluck: db | getAllModify (pluck (true)),
    iterate: db | getIterate,
    get: db | getGet,
    getExpand: db | getGetModify (expand (true)),
    getRaw: db | getGetModify (raw (true)),
    getPluck: db | getGetModify (pluck (true)),
    getPluckFail: (db | getGetModify (pluck (true))) >> flatMap (
      ifDefined (id >> Right, () => Left ('empty result for sql query')),
    ),
    pragmaSimple: db | getPragmaSimple,
    pragma1Simple: db | getPragma1Simple,
    pragmaWithOpts: db | getPragmaWithOpts,
    pragma1WithOpts: db | getPragma1WithOpts,
    pragma: db | getPragma,
    pragma1: db | getPragma1,
  }),
)
