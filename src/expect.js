// --- expect is a global symbol (provided by jest) and eslint knows about it.

import {
  pipe, compose, composeRight,
  recurry,
  dot1, dot, prop, path,
  passTo, getType,
  invoke, tap,
} from 'stick-js/es'

import { then, recover, } from './async.js'

// --- to-be is reference equality; to-equal matches similar objects.

export const toEqual = /*#__PURE__*/ 'toEqual' | dot1
export const toBe = /*#__PURE__*/ 'toBe' | dot1
export const toThrow = /*#__PURE__*/ 'toThrow' | dot
export const toThrowA = /*#__PURE__*/ 'toThrow' | dot1
export const not = /*#__PURE__*/ 'not' | prop
export const toBeInstanceOf = /*#__PURE__*/ 'toBeInstanceOf' | dot1
export const toBeTruthy = /*#__PURE__*/ 'toBeTruthy' | dot
export const toBeFalsy = /*#__PURE__*/ 'toBeFalsy' | dot
export const rejects = /*#__PURE__*/ 'rejects' | prop
export const resolves = /*#__PURE__*/ 'resolves' | prop

// --- anything is a function.
export const { anything, } = expect

export const expectToEqual = /*#__PURE__*/ recurry (2) (
  expected => expect >> toEqual (expected),
)

export const expectToBe = /*#__PURE__*/ recurry (2) (
  expected => expect >> toBe (expected),
)

export const expectNotToEqual = /*#__PURE__*/ recurry (2) (
  expected => expect >> not >> toEqual (expected),
)

export const expectNotToBe = /*#__PURE__*/ recurry (2) (
  expected => expect >> not >> toBe (expected),
)

export const expectOk = /*#__PURE__*/ expectToEqual (anything ())
export const expectNotOk = /*#__PURE__*/ expectNotToEqual (anything ())
export const expectNil = /*#__PURE__*/ expectNotOk

export const expectToMatchRegex = /*#__PURE__*/ recurry (2) (
  regex => expectToEqual (expect.stringMatching (regex)),
)

export const expectNotToMatchRegex = /*#__PURE__*/ recurry (2) (
  regex => expectNotToEqual (expect.stringMatching (regex)),
)

export const expectToBeInstanceOf = /*#__PURE__*/ recurry (2) (
  expected => expect >> toBeInstanceOf (expected),
)

export const expectNotToBeInstanceOf = /*#__PURE__*/ recurry (2) (
  expected => expect >> not >> toBeInstanceOf (expected),
)

// --- checks constructor.name.
export const expectConstructorNameToBe = /*#__PURE__*/ recurry (2) (
  expected => path (['constructor', 'name']) >> expectToBe (expected),
)

// --- checks constructor.name.
export const expectConstructorNameNotToBe = /*#__PURE__*/ recurry (2) (
  expected => path (['constructor', 'name']) >> expectNotToBe (expected),
)

export const expectToBeTruthy = /*#__PURE__*/ expect >> toBeTruthy
export const expectToBeFalsy = /*#__PURE__*/ expect >> toBeFalsy

// --- tests on truthy.
export const expectPredicate = /*#__PURE__*/ recurry (2) (
  p => passTo (p) >> expectToBeTruthy,
)

// --- tests on falsy.
export const expectNotPredicate = /*#__PURE__*/ recurry (2) (
  p => passTo (p) >> expectToBeFalsy,
)

const typeOf = /*#__PURE__*/ x => typeof x

// ------ using JS 'typeof' operator.

export const expectToHaveTypeof = /*#__PURE__*/ recurry (2) (
  expected => typeOf >> expectToBe (expected),
)

export const expectNotToHaveTypeof = /*#__PURE__*/ recurry (2) (
  expected => typeOf >> expectNotToBe (expected),
)

// ------ using stick-js 'getType' function, which works by calling Object.prototype.toString and
// parsing the result.

export const expectToHavePrimitiveType = /*#__PURE__*/ recurry (2) (
  expected => getType >> expectToBe (expected),
)

export const expectNotToHavePrimitiveType = /*#__PURE__*/ recurry (2) (
  expected => getType >> expectNotToBe (expected),
)

// ------ true if expected is a subset of received.

export const expectToContainObject = /*#__PURE__*/ recurry (2) (
  expected => expectToEqual (expect.objectContaining (expected)),
)

export const expectNotToContainObject = /*#__PURE__*/ recurry (2) (
  expected => expectNotToEqual (expect.objectContaining (expected)),
)

// ------ these return a promise: make sure your test returns a promise or uses `async`/`await`.

// --- expectToRejectWith is generally easier to use.
export const expectToRejectWithF = /*#__PURE__*/ recurry (2) (
  catcher => invoke >> then (
    () => false | expectToBe (true),
  ) >> recover (catcher)
)

// --- there is no generic 'expectToReject' function because it's too generic (runtime errors might
// pass for example).
export const expectToRejectWith = /*#__PURE__*/ recurry (2) (
  rejectValue => invoke >> expect >> rejects >> toEqual (rejectValue),
)

/* old implementation, possibly instructive.
export const expectNotToReject = #__PURE__ invoke
  >> then (
    () => false | expectToBe (false),
  ) >> recover (
    () => false | expectToBe (true),
  )
*/

export const expectToResolve = /*#__PURE__*/ invoke >> expect >> resolves >> toEqual (anything ())
export const expectToResolveWith = /*#__PURE__*/ recurry (2) (
  resolveValue => invoke >> expect >> resolves >> toEqual (resolveValue),
)

export const expectToThrow = /*#__PURE__*/ expect >> toThrow
export const expectToThrowA = /*#__PURE__*/ recurry (2) (
  errorType => expect >> toThrowA (errorType),
)
export const expectNotToThrow = /*#__PURE__*/ expect >> not >> toThrow
