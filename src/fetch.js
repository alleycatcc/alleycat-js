import {
  pipe, compose, composeRight,
  whenOk, tap, map, ifPredicate,
  eq, F, T, noop, condS, otherwise, guard,
  againstAny, always, prop, id, recurry,
  ifOk, merge, concat, concatTo,
} from 'stick-js/es'

import { letsP, } from './lets-promise.js'

import daggy from 'daggy'
import 'whatwg-fetch'

import { recover, rejectP, } from './async.js'
import { Just, Nothing, toMaybe, cata, fold, fold3, foldWhenJust, foldMaybe, } from './bilby.js'
import { logWith, between, iwarn, } from './general.js'
import { EffNoEffect, } from './saga.js'

import {
  ResultOk, ResultUserError, ResultNonUserError,
  resultFoldMap, resultFold,
} from './internals/types.js'

export { resultFoldMap, resultFold, }

// --- JSON parsing is skipped for the following status codes.
export const noParseCodes = /*#__PURE__*/ (codes) => againstAny (codes | map (eq))

// --- note: res.json () returns a promise.
const parseJSON = /*#__PURE__*/ (noParse) => (res) => {
  return res.status | ifPredicate (noParse) (
    () => null,
    () => res.json (),
  )
}

export const defaultOpts = /*#__PURE__*/ {
  headers: {
    'Content-Type': 'application/json',
  },
  credentials: 'include',
}

/**
 * These functions are for calling a JSON api.
 *
 * We will parse the response returned by `requestJSON` as JSON regardless of the status code.
 * Note that 'Content-Type: application/json' is not set here, but is given as a (default) option to
 * `doApiCall`.
 *
 * If the request fails or the JSON doesn't parse, reject and then resolve with `ResultNonUserError`
 * containing a `Just String`.
 * If the status is ok, resolve with `ResultOk`.
 * If the status is 4xx and there's a `umsg`, resolve with `ResultUserError`.
 * Otherwise, resolve with `ResultNonUserError`, whose contents will be `Just String` or `Nothing`
 * depending on whether there is an `imsg`.
 */

/**
 * In this configuration it will only resolve with Ok or NonUserError (because `bodyGetUmsg`) is
 * empty.
 */
export const requestJSON = /*#__PURE__*/ recurry (3) (
  ({
    codeCondOk=between (200, 299),
    codeCondUE=between (400, 499),
    bodyGetUmsg=noop, bodyGetImsg=noop, bodyGetCustom=noop,
    noParse=noParseCodes ([204, 205]),
  }) => url => options => letsP (
    () => fetch (url, options),
    (res) => res | parseJSON (noParse),
    (res, body) => [res.status, body | bodyGetUmsg, body | bodyGetImsg, body | bodyGetCustom],
    (_res, body, [code, umsg=null, imsg=null, custom=null]) => code | condS ([
      codeCondOk | guard (() => ResultOk (body)),
      codeCondUE | guard (() => ResultUserError (umsg, custom)),
      otherwise | guard (() => rejectP (imsg, custom)),
    ]),
  )
  | recover ((imsg=null, custom=null) => ResultNonUserError (imsg | toMaybe, custom))
)

/**
 * A 'standard' call for most of our APIs, which set a 'umsg' property for a user error, and an
 * optional 'imsg' property for an internal error.
 */

export const requestJSONStdOpts = /*#__PURE__*/ (opts) => requestJSON ({
  bodyGetUmsg: (body) => body | whenOk (prop ('umsg')),
  bodyGetImsg: (body) => body | whenOk (prop ('imsg')),
  bodyGetCustom: (body) => body | whenOk (prop ('custom')),
  ... opts,
})

export const requestJSONStd = /*#__PURE__*/ requestJSONStdOpts ({})

export const Request = /*#__PURE__*/ daggy.taggedSum ('Request', {
  RequestInit: [],
  // --- combiner is a function, e.g. `concat`, telling us how to combine new results with existing
  // ones -- useful for e.g. an infinite scroll loading more results.
  // --- mbCombiner looks like `Maybe [currentResults, combiner, tag]`, where tag is a string or a
  // datatype which can be used to find out what this combiner will do.
  RequestLoading: ['mbCombiner'],
  // --- may also include the case where the request was good but contained an error field.
  RequestError: ['reason'],
  RequestResults: ['results'],
})

const { RequestInit, RequestLoading, RequestError, RequestResults, } = Request
export { RequestInit, RequestLoading, RequestError, RequestResults, }

export const requestIsInit = /*#__PURE__*/ cata ({
  RequestInit: T,
  RequestLoading: F,
  RequestError: F,
  RequestResults: F,
})

export const requestIsPending = /*#__PURE__*/ cata ({
  RequestInit: F,
  RequestLoading: T,
  RequestError: F,
  RequestResults: F,
})

export const requestIsResults = /*#__PURE__*/ cata ({
  RequestInit: F,
  RequestLoading: F,
  RequestError: F,
  RequestResults: T,
})

export const foldWhenRequestResults = /*#__PURE__*/ recurry (2) (
  (f) => cata ({
    RequestInit: noop,
    RequestLoading: noop,
    RequestError: noop,
    RequestResults: f,
  }),
)

export const foldIfRequestResults = /*#__PURE__*/ recurry (3) (
  (yes) => (no) => cata ({
    RequestInit: no,
    RequestLoading: no,
    RequestError: no,
    RequestResults: yes,
  }),
)

Request.prototype.fold = function (f, y) {
  const z = y | always
  return this | cata ({
    RequestInit: z,
    RequestLoading: (combinerMb) => combinerMb | fold (
      ([results, ..._]) => results | f,
      y,
    ),
    RequestError: z,
    RequestResults: f,
  })
}

Request.prototype.map = function (f) {
  return this | cata ({
    RequestInit: () => RequestInit,
    RequestLoading: RequestLoading,
    RequestError: RequestError,
    RequestResults: f >> RequestResults,
  })
}

// :: data RequestComplete = RequestCompleteSuccess { results :: * }
//                         | RequestCompleteError { mbUmsg :: Maybe String }
const RequestComplete = daggy.taggedSum ('RequestComplete', {
  RequestCompleteSuccess: ['results'],
  RequestCompleteError: ['mbUmsg'],
})

const { RequestCompleteSuccess, RequestCompleteError, } = RequestComplete
export { RequestCompleteSuccess, RequestCompleteError, }

// --- @todo move to internal/types?

/*
 * `f`: for RequestCompleteSuccess (arg = results)
 * `g`: for RequestCompleteError signifying a user error (arg = umsg)
 * `h`: for RequestCompleteError signifying a server error (no arg)
 */
RequestComplete.prototype.fold = function (f, g, h) {
  return this | cata ({
    RequestCompleteSuccess: f,
    RequestCompleteError: (mbUmsg) => mbUmsg | foldMaybe (
      (umsg) => g (umsg),
      () => h (),
    ),
  })
}

export const requestCompleteFold = /*#__PURE__*/ fold3

// --- inject the redux-saga/effects module to solve weird import issues.
function *_doApiCall (
  sagaEffects,
  {
    url,
    // --- this is where you set `headers: { contentType: ..., }`, `method`, `body` etc.
    optsMerge={},
    request=requestJSONStd,
    resultsModify=id,
    continuation=EffNoEffect,
    oops=noop,
    oopsOnUserError=true,
    oopsOnNonUserError=true,
    imsgDecorate=null,
  }
) {
  const { all, call, put, } = sagaEffects
  const opts = defaultOpts | merge ({
    ...optsMerge,
  })
  const results = yield call (request, url, opts)
  const onCompleteEffect = (payload) => continuation | cata ({
    EffAction: (action) => put (payload | action),
    EffSaga: (f) => call (f, payload),
    EffNoEffect: () => null,
  })
  yield all (results
    | resultsModify
    | resultFold (
      // --- ok (code 200-299 by default)
      (result) => [RequestCompleteSuccess (result) | onCompleteEffect],
      // --- user error (code 400-499 by default), `umsg` may be nil
      (umsg=null, custom=null) => [
        ... oopsOnUserError ? [call (oops, umsg, custom)] : [],
        RequestCompleteError (Just (umsg)) | onCompleteEffect,
      ],
      // --- non-user error (other codes by default)
      (imsgMb) => {
        imsgMb | foldWhenJust (
          concatTo (imsgDecorate | ifOk (
            concat (' '),
            () => '',
          )) >> iwarn,
        )
        return [
          ... oopsOnNonUserError ? [call (oops, 'Oops! Something went wrong')] : [],
          RequestCompleteError (Nothing) | onCompleteEffect,
        ]
      },
    )
  )
}

export const doApiCall = /*#__PURE__*/ _doApiCall
